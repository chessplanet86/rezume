def main():
    n = int(input())
    arr = [int(x) for x in input().split()]
    arr = sorted(arr)
    if n == 2:
        print(arr[1] - arr[0])
        return

    dp = [arr[1] - arr[0], arr[2] - arr[0]]
    for i in range(3, n):
        d = arr[i] - arr[i - 1]
        m = min(dp[-1], dp[-2])
        res = d + m
        dp.append(res)

    print(dp[-1])


if __name__ == '__main__':
    main()
