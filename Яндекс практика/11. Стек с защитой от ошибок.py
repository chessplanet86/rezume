class Stack:
    def __init__(self):
        self.arr = []

    def _error(self, func):
        if self.arr:
            return func(self.arr)
        else:
            return 'error'

    def executor(self, fn_name, *args):
        return getattr(self, fn_name)(*args)

    def clear(self):
        self.arr = []
        return 'ok'

    @staticmethod
    def _back(arr):
        return arr[-1]

    def back(self):
        return self._error(self._back)

    def size(self):
        return len(self.arr)

    def exit(self):
        return 'bye'

    def push(self, a):
        self.arr.append(int(a))
        return 'ok'

    @staticmethod
    def _pop(arr):
        return arr.pop()

    def pop(self):
        return self._error(self._pop)


def main():
    stack = Stack()
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    command = fp.readline()

    while command:
        res = stack.executor(*command.split())
        fw.write(str(res) + '\n')
        command = fp.readline()
        if res == 'bye':
            break

    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
