class Graph:
    def __init__(self, vertexs, arr):
        _range = range(vertexs)
        self.colors = [0 for _ in _range]
        self.adjacency_list = {i: [] for i in _range}
        for i in _range:
            for j in _range:
                if arr[i][j]:
                    self.adjacency_list[i].append(j)

    def search_cicle(self):
        for node in self.adjacency_list:
            if self.colors[node] == 0:
                if res := self.dfs(node, self.colors, self.adjacency_list):
                    return res

    @staticmethod
    def dfs(start_v: int, colors: list, adjacency_list: list) -> bool:
        stack = []
        path = []
        stack.append(start_v)
        parent = {}
        while stack:
            v = stack.pop()
            if colors[v] == 0:
                colors[v] = 1
                stack.append(v)
                path.append(str(v + 1))
                for w in adjacency_list[v]:
                    if colors[w] == 0:
                        stack.append(w)
                        parent[w] = v
                    elif colors[w] == 1 and parent.get(v) == w:
                        pass
                    elif colors[w] == 1 and parent.get(v) != w:
                        path.append(str(w + 1))
                        return path
            elif colors[v] == 1:
                colors[v] = 2
                path.pop()


def main():
    fp = open('input.txt', 'r', encoding='utf-8')
    vertexs = int(fp.readline())
    _range = range(vertexs)
    arr = [None for _ in _range]
    for i in _range:
        arr[i] = [int(x) for x in fp.readline().split(' ')]

    graph = Graph(vertexs, arr)
    fw = open('output.txt', 'w')
    if res := graph.search_cicle():
        last = res[-1]
        i = -2
        r = f'{res[-1]}'
        a = res[i]
        k = 1
        while a != last:
            r += f' {a}'
            k += 1
            i -= 1
            a = res[i]

        fw.write('YES' + '\n')
        fw.write(str(k) + '\n')
        fw.write(r)
    else:
        fw.write('NO')
    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
