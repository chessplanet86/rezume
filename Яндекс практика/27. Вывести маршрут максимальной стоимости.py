def main():
    res = []
    n, m = [int(x) for x in input().split()]
    arr = [[None for _ in range(m)] for _ in range(n)]
    for i in range(n):
        row = [int(x) for x in input().split()]
        for j in range(m):
            arr[i][j] = row[j]

    w = [[float('-inf') for _ in range(m + 1)] for _ in range(n + 1)]
    w[0][0] = 0
    w[0][1] = 0
    w[1][0] = 0

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            w[i][j] = max(w[i - 1][j], w[i][j - 1]) + arr[i - 1][j - 1]

    x = -1
    y = -1
    while not (x - 1 == -n - 1 and y - 1 == -m - 1):
        maximum = max(w[x - 1][y], w[x][y - 1])
        if maximum == w[x - 1][y]:
            x = x - 1
            res.append('D')
        elif maximum == w[x][y - 1]:
            y = y - 1
            res.append('R')

    res.reverse()
    print(w[-1][-1])
    print(' '.join(res))


if __name__ == '__main__':
    main()
