simbols = dict()
arr = []
fp = open('input.txt', 'r')
line = fp.readline()
while line:
    line = line.strip()
    for s in line:
        if s == ' ':
            continue
        if simbols.get(s):
            simbols[s] += 1
        else:
            simbols[s] = 1
    line = fp.readline()

simbols = dict(sorted(simbols.items()))

a = ''
for key in simbols:
    a += key

arr.append(a)

is_empty = False
while not is_empty:
    is_empty = True
    a = ''
    for key in simbols:
        if simbols[key] == 0:
            a += ' '
        else:
            a += '#'
            simbols[key] -= 1
            is_empty = False
    arr.append(a)
with open('output.txt', 'w') as f:
    for i in range(len(arr) - 2, -1, -1):
        f.write(arr[i]+'\n')
fp.close()
