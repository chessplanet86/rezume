def main():
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    n = int(fp.readline())
    vagons = dict()
    products = []
    counts = []
    for i in range(n):
        line = fp.readline()
        line = line.split()
        command = line[0]
        if command == 'add':
            if vagons.get(line[2]):
                vagons[line[2]] += int(line[1])
            else:
                vagons[line[2]] = int(line[1])
            products.append(line[2])
            counts.append(int(line[1]))
        if command == 'get':
            if vagons.get(line[1]):
                fw.write(str(vagons[line[1]]) + '\n')
            else:
                fw.write('0' + '\n')

        if command == 'delete':
            count_delete = int(line[1])
            while count_delete != 0:
                top_vagons = counts.pop()
                delta = top_vagons - count_delete
                if delta > 0:
                    counts.append(delta)
                    product = products.pop()
                    vagons[product] = vagons[product] - top_vagons + delta
                    products.append(product)
                    break
                if delta == 0:
                    product = products.pop()
                    vagons[product] -= count_delete
                    break
                if delta < 0:
                    count_delete = abs(delta)
                    product = products.pop()
                    vagons[product] -= top_vagons

    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
