def DFS(start_v):
    stack = []
    stack.append(start_v)
    parent = {}
    while stack:
        v = stack.pop()
        if colors[v] == 0:
            colors[v] = 1
            stack.append(v)
            for w in nodes[v]:
                if colors[w] == 0:
                    stack.append(w)
                elif colors[w] == 1 and parent.get(v) == w:
                    pass
                elif colors[w] == 1 and parent.get(v) != w:
                    return -1
        elif colors[v] == 1:
            colors[v] = 2
            order.append(str(v + 1))


if __name__ == '__main__':

    """
    0 - белый цвет вершины
    1 - серый цвет вершины
    2 - черный цвет вершины
    """
    node, edge = [int(x) for x in input().split(' ')]
    r = range(node)
    colors = [0 for _ in r]
    edges = []
    nodes = {i: [] for i in r}

    for _ in range(edge):
        x, y = [int(x) - 1 for x in input().split(' ')]
        if nodes.get(x):
            nodes[x].append(y)
        else:
            nodes[x] = [y]

    for i in nodes:
        nodes[i] = sorted(nodes[i])[::-1]

    start_vertex = 2
    order = []
    a = None
    for node in nodes:
        if colors[node] == 0:
            if DFS(node) == -1:
                a = -1
                break
    if a == -1:
        print(-1)
    else:
        print(' '.join(order[::-1]))
