def get_answer_mask(text: str) -> list[int]:
    from collections import Counter
    resp, mask = text.split(', ')
    counter = Counter(mask)
    arr = []
    resp_list = list(resp)
    mask_list = list(mask)
    for i in range(5):
        if resp[i] == mask[i]:
            arr.append('1')
            resp_list[i] = None
            mask_list[i] = None

        elif resp[i] != mask[i]:
            arr.append(None)

    counter = Counter(mask_list)
    for i in range(5):
        if arr[i] == '1':
            continue
        elif counter[resp[i]] > 0:
            arr[i] = '0'
            counter[resp[i]] -= 1
        else:
            arr[i]='-1'
    return ', '.join(arr)


if __name__ == "__main__":
    input_str = input()
    # Необходимо преобразовать список в строку перед выводом.
    print(get_answer_mask(input_str))