def compress_sequence(text: str) -> list[tuple[int, int]]:
    from collections import Counter
    arr = text.split(', ')
    zip = [*arr]
    last = ''
    count = 1
    s = ''
    for i in range(len(arr) - 1, 0, -1):
        if arr[i] == arr[i - 1]:
            count += 1
            zip[i] = None

        else:
            zip[i] = count
            count = 1
    if arr[0] == arr[1]:
        zip[0] = count + 1
    else:
        zip[0] = 1
    for i in range(len(arr)):
        if zip[i]:
            s += f'({arr[i]}, {zip[i]}) '
    return s


if __name__ == "__main__":
    input_str = input()
    # Необходимо преобразовать список в строку перед выводом.
    print(compress_sequence(input_str))
