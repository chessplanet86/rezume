def main():
    cnt_vertexes, cnt_edges = [int(x) for x in input().split()]
    print('YES' if is_red_black_graph(cnt_vertexes,
                                      [tuple(map(int, input().split())) for _ in range(cnt_edges)]) else 'NO')


def init_graph(vertexes: int, edges: list) -> dict:
    res = {k: set() for k in range(1, vertexes + 1)}
    for fst_vertex, scd_vertex in edges:
        res.get(fst_vertex).add(scd_vertex)
        res.get(scd_vertex).add(fst_vertex)
    return res


def dfs(g, p, n, current_color):
    p[n] = current_color
    for neighbour in g[n]:
        if p[neighbour] == 0:
            if not dfs(g, p, neighbour, 3 - current_color):
                return False
        elif p[neighbour] == current_color:
            return False
    return True


def is_red_black_graph(count_vertexes: int, edges: list) -> bool:
    graph = init_graph(count_vertexes, edges)
    parts = dict.fromkeys(graph.keys(), 0)
    for vertex in graph.keys():
        if parts[vertex] == 0:
            if not dfs(graph, parts, vertex, 1):
                return False
    return True


if __name__ == '__main__':
    main()
