def main_simple():
    n = int(input())
    if n == 1:
        print(0)
    else:
        counter = 0
        last = int(input())
        for _ in range(n - 1):
            new = int(input())
            if new >= last:
                counter += last
            else:
                counter += new
            last = new
        print(counter)



def main():
    N = int(input())
    letters = dict()

    for i in range(N):
        s = int(input())
        letters[i] = s

    count = 1
    total = 0

    last = - 10
    while count:
        count = 0

        for key in letters:
            if letters[key] > 0:
                count += 1 if key - last == 1 else 0
                last = key
                letters[key] -= 1
            else:
                count = 0
        total += count

    print(total)


if __name__ == '__main__':
    main()
