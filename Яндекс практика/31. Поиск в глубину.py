def main():
    arr = []

    def DFS(start_v):
        stack = []
        stack.append(start_v)
        while stack:
            v = stack.pop()
            if colors[v] == 0:
                arr.append(v + 1)
                colors[v] = 1
                stack.append(v)
                for w in nodes[v]:
                    try:
                        if colors[w] == 0:
                            stack.append(w)
                    except:
                        print(colors)
                        print(w)
                        break
            elif colors[v] == 1:
                colors[v] = 2

    """
    0 - белый цвет вершины
    1 - серый цвет вершины
    2 - черный цвет вершины
    """
    fp = open('input.txt', 'r')
    node, edge = [int(x) for x in fp.readline().split(' ')]
    colors = [0 for _ in range(node)]
    nodes = {i: [] for i in range(node)}

    for _ in range(edge):
        x, y = [int(x) - 1 for x in fp.readline().split(' ')]
        if nodes.get(x):
            nodes[x].append(y)
        else:
            nodes[x] = [y]

        if nodes.get(y):
            nodes[y].append(x)
        else:
            nodes[y] = [x]

    fp.close()
    for i in nodes:
        nodes[i] = sorted(nodes[i])[::-1]

    start_vertex = 0

    DFS(start_vertex)
    arr = sorted(arr)
    fw = open('output.txt', 'w')
    fw.write(str(len(arr)) + '\n')
    res = ''
    for i in arr:
        res += f'{i} '

    fw.write(res)
    fw.close()


if __name__ == '__main__':
    main()
