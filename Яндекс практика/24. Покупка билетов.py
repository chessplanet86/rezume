def main():
    n = int(input())
    a = [0] * 5001
    b = [0] * 5001
    c = [0] * 5001
    for i in range(1, n + 1):
        a[i], b[i], c[i] = [int(x) for x in input().split()]

    time = [0] * (n + 1)
    time[0] = 0
    time[1] = a[1]
    if n > 1:
        time[2] = min(a[1] + a[2], b[1])

    for i in range(3, n + 1):
        time[i] = min(time[i - 1] + a[i], time[i - 2] + b[i - 1], time[i - 3] + c[i - 2])
    print(time[-1])


if __name__ == '__main__':
    main()
