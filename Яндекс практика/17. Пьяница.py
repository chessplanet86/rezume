import queue


def main():
    player_1 = queue.Queue()
    player_2 = queue.Queue()
    [player_1.put(int(i)) for i in input().split()]
    [player_2.put(int(i)) for i in input().split()]
    k = 0
    while not player_1.empty() and not player_2.empty():
        k += 1
        card_1 = player_1.get()
        card_2 = player_2.get()

        if card_1 == 9 and card_2 == 0:
            player_2.put(card_1)
            player_2.put(card_2)
            continue
        elif card_2 == 9 and card_1 == 0:
            player_1.put(card_1)
            player_1.put(card_2)
            continue

        if card_1 > card_2:
            player_1.put(card_1)
            player_1.put(card_2)
        else:
            player_2.put(card_1)
            player_2.put(card_2)

    if k < 1000000:
        if not player_1.empty():
            print('first', k)
        else:
            print('second', k)
    else:
        print('botva')


if __name__ == '__main__':
    main()
