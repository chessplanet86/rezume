def main():
    def DFS(start_v):
        stack = []
        family = []
        stack.append(start_v)
        while stack:
            v = stack.pop()
            if colors[v] == 0:
                colors[v] = 1
                stack.append(v)
                for w in nodes[v]:
                    if colors[w] == 0:
                        stack.append(w)
            elif colors[v] == 1:
                colors[v] = 2
                family.append(v + 1)

        return sorted(family)

    """
        0 - белый цвет вершины
        1 - серый цвет вершины
        2 - черный цвет вершины
        """
    node, edge = [int(x) for x in input().split(' ')]
    r = range(node)
    colors = [0 for _ in r]
    edges = []
    nodes = {i: [] for i in r}

    for _ in range(edge):
        x, y = [int(x) - 1 for x in input().split(' ')]
        if nodes.get(x):
            nodes[x].append(y)
        else:
            nodes[x] = [y]

        if nodes.get(y):
            nodes[y].append(x)
        else:
            nodes[y] = [x]

    for i in nodes:
        nodes[i] = sorted(nodes[i])[::-1]

    order = []
    all_components = ''
    conn_comp = 0
    for node in nodes:
        if colors[node] == 0:
            conn_comp += 1
            res = DFS(node)
            length = len(res)
            res = ' '.join(map(str, res))
            all_components += '\n' + str(length) + '\n' + res

    print(conn_comp, end='')
    print(all_components)


if __name__ == '__main__':
    main()
