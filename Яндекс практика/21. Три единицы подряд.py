def main():
    n = int(input())
    if n == 1:
        print(2)
        return
    if n == 2:
        print(4)
        return
    if n == 3:
        print(7)
        return
    arr = [0 for _ in range(n)]
    arr[0] = 2
    arr[1] = 4
    arr[2] = 7
    for i in range(3, n):
        arr[i] = arr[i - 1] + arr[i - 2] + arr[i - 3]

    print(arr[n-1])


if __name__ == '__main__':
    main()
