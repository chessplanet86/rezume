def main():
    string = input()
    dict_hooks = {
        '(': 1,
        ')': 1,
        '[': 2,
        ']': 2,
        '{': 3,
        '}': 3
    }
    if is_correct_bracket_seq(string, dict_hooks):
        print('yes')
    else:
        print('no')


def is_correct_bracket_seq(string, dict_hooks):
    arr = []
    for i in string:

        if i in {'(', '[', '{'}:
            arr.append(i)
        else:
            if len(arr) != 0:
                pop_symb = arr.pop()
                if dict_hooks[pop_symb] != dict_hooks[i]:
                    return False
            else:
                return False
    return not bool(len(arr))


if __name__ == '__main__':
    main()
