def get_sec(time):
    return 3600 * time[0] + 60 * time[1] + time[2]


def main():
    time_sent = [int(i) for i in input().split(':')]
    time_server = [int(i) for i in input().split(':')]
    time_in = [int(i) for i in input().split(':')]

    limit = 24 * 60 * 60
    if get_sec(time_sent) <= get_sec(time_in):
        dif = round((get_sec(time_in) - get_sec(time_sent)) / 2 + 0.0001)
    else:
        dif = round((limit - get_sec(time_sent) + get_sec(time_in)) / 2 + 0.0001)

    new_time = (get_sec(time_server) + dif) % limit

    if new_time // 3600 < 10:
        hh = f'0{new_time // 3600}'
    else:
        hh = new_time // 3600

    if new_time % 3600 // 60 < 10:
        mm = f'0{new_time % 3600 // 60}'
    else:
        mm = new_time % 3600 // 60

    if new_time % 3600 % 60 < 10:
        ss = f'0{new_time % 3600 % 60}'
    else:
        ss = new_time % 3600 % 60
    print(f'{hh}:{mm}:{ss}')


if __name__ == '__main__':
    main()
