def main():
    n = int(input())
    diego = sorted(list({int(x) for x in input().split()}))
    col = input()
    cards = [int(x) for x in input().split()]

    for card in cards:
        if card > diego[-1]:
            print(n)
        elif card <= diego[0]:
            print(0)
        else:
            res = bin_search(diego, 0, n, card)
            if card != diego[res]:
                print(res + 1)
            else:
                if res - 1 > 0:
                    print(res - 1)
                else:
                    pass


def bin_search(arr, beg, end, k):
    mid = (beg + end) // 2

    if mid < len(arr) and arr[mid] == k:
        return mid
    elif mid == len(arr):
        return end - 1

    if beg == end == 0 or beg == end == len(arr):
        if arr[beg - 1] < k:
            return beg - 1
        else:
            return 0
    elif beg == end:
        if arr[beg - 1] < k:
            return beg - 1
        else:
            return 0

    if arr[mid] > k:
        end = mid
    elif arr[mid] < k:
        beg = mid + 1
    return bin_search(arr, beg, end, k)


if __name__ == '__main__':
    main()
