import sys


class HeapMax:
    def __init__(self, size):
        self.arr = [None for _ in range(size)]
        self.head = 0

    def append(self, x):
        if self.head == 0:
            self.arr[self.head] = x
            self.head += 1
        else:
            self.arr[self.head] = x
            current_idx = self.head
            parent_idx = (self.head - 1) // 2
            while current_idx != 0 and self.arr[current_idx] > self.arr[parent_idx]:
                self.arr[current_idx], self.arr[parent_idx] = self.arr[parent_idx], self.arr[current_idx]
                current_idx = parent_idx
                parent_idx = (current_idx - 1) // 2

            self.head += 1

    def print(self):
        print(self.arr)

    @staticmethod
    def _max(x, y):
        if x is None and y is not None:
            return y
        elif x is not None and y is None:
            return x
        elif x is None and y is None:
            return -sys.maxsize
        else:
            return max(x, y)

    def get_max(self):
        max = self.arr[0]
        x = self.arr[self.head - 1]
        self.arr[self.head - 1] = None
        self.head -= 1
        self.arr[0] = x
        parent_idx = 0
        childe_one = 2 * parent_idx + 1
        childe_two = 2 * parent_idx + 2
        while self.arr[parent_idx] < self._max(self.arr[childe_one], self.arr[childe_two]):
            if self._max(self.arr[childe_one], self.arr[childe_two]) == self.arr[childe_one]:
                self.arr[parent_idx], self.arr[childe_one] = self.arr[childe_one], self.arr[parent_idx]
                parent_idx = childe_one
            elif self._max(self.arr[childe_one], self.arr[childe_two]) == self.arr[childe_two]:
                self.arr[parent_idx], self.arr[childe_two] = self.arr[childe_two], self.arr[parent_idx]
                parent_idx = childe_two
            childe_one = 2 * parent_idx + 1
            childe_two = 2 * parent_idx + 2
        return max


def main():
    heap = HeapMax(100000)
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    n = int(fp.readline())
    k = 0
    while k < n:
        command = fp.readline()
        s = command.split()
        if len(s) == 2:
            heap.append(int(s[1]))
        else:
            res = heap.get_max()
            fw.write(str(res) + '\n')
        k += 1

    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
