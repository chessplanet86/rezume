def sort(arr):
    stack = []
    depo = 0
    last_length_stack = -1
    flag = True
    while flag:
        if stack:
            a = stack.pop()
            if depo + 1 == a:
                depo = a
            else:
                stack.append(a)
                if not arr:
                    return depo
                while len(arr):
                    b = arr.pop()
                    if b == depo + 1:
                        depo = b
                        break
                    else:
                        stack.append(b)

        elif arr:
            while len(arr):
                a = arr.pop()
                if a == depo + 1:
                    depo = a
                    break
                else:
                    stack.append(a)
        else:
            if len(stack):
                a = stack.pop()
            if a == depo + 1:
                depo = a
            break

    return depo


def main():
    n = int(input())
    arr = [int(x) for x in input().split()]
    arr.reverse()

    if sort(arr) == n:
        print('YES')
    else:
        print('NO')


if __name__ == '__main__':
    main()
