def main():
    arr = [x for x in input().split()]
    stack = []
    for i in arr:
        if i in {'+', '-', '*'}:
            top = stack.pop()
            bottom = stack.pop()
            res = eval(f'{bottom}{i}{top}')
            stack.append(res)
        else:
            stack.append(i)

    print(stack.pop())


if __name__ == '__main__':
    main()
