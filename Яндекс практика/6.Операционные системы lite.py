def main():
    m = input()
    n = int(input())
    arr = []
    if n == 0:
        print(0)
        return

    append = arr.append
    for i in range(n):
        a, b = [int(i) for i in input().split()]
        append(a)
        append(b)

    calc_arr = [arr[0], arr[1]]
    append = calc_arr.append

    for i in range(3, len(arr), 2):
        for j in range(1, len(calc_arr), 2):
            if (calc_arr[j - 1] <= arr[i] <= calc_arr[j]
                    or calc_arr[j - 1] <= arr[i - 1] <= calc_arr[j]
                    or arr[i - 1] <= calc_arr[j] <= arr[i]
                    or arr[i - 1] <= calc_arr[j] <= arr[i]
            ):
                calc_arr[j - 1] = -1
                calc_arr[j] = -1
        append(arr[i - 1])
        append(arr[i])

    k = 0
    for i in calc_arr:
        if i > 0:
            k += 1
    print(k // 2)


if __name__ == '__main__':
    main()
