def main():
    n, m = [int(x) for x in input().split()]
    dp = [[0 for _ in range(m + 2)] for _ in range(n + 2)]
    dp[2][2] = 1
    for i in range(3, n + 2):
        for j in range(2, m + 2):
            dp[i][j] = dp[i - 1][j - 2] + dp[i - 2][j - 1]

    print(dp[-1][-1])


if __name__ == '__main__':
    main()
