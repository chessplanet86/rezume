class Deque:
    def __init__(self, max_size):
        self.max_size = max_size
        self.arr = [None for _ in range(max_size)]
        self._size = 0
        self.head = 0

    def executor(self, fn_name, *args):
        return getattr(self, fn_name)(*args)

    def push_front(self, x):
        self.arr[self.head] = x
        self.head = (self.head + 1) % self.max_size
        self._size += 1
        return 'ok'

    def push_back(self, x):
        self.arr[(self.head - self._size - 1) % self.max_size] = x
        self._size += 1
        return 'ok'

    def pop_front(self):
        x = self.arr[(self.head - 1) % self.max_size]
        self.head = (self.head - 1) % self.max_size
        if x is None:
            return 'error'
        self.arr[self.head] = None
        self._size -= 1
        return x

    def pop_back(self):
        x = self.arr[self.head - self._size]
        if x is None:
            return 'error'
        self.arr[self.head - self._size] = None
        self._size -= 1
        return x

    def front(self):
        if self._size:
            return self.arr[self.head - 1]
        else:
            return 'error'

    def back(self):
        if self._size:
            return self.arr[self.head - self._size]
        else:
            return 'error'

    def size(self):
        return self._size

    def clear(self):
        self.arr = [None for _ in range(self.max_size)]
        self._size = 0
        self.head = 0
        return 'ok'

    def exit(self):
        return 'bye'


def main():
    q = Deque(200)
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    command = fp.readline()

    while command:
        res = q.executor(*command.split())
        fw.write(str(res) + '\n')
        command = fp.readline()
        if res == 'bye':
            break

    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
