def main():
    n = int(input())
    arr = input().split()
    for i in range(len(arr)):
        arr[i] = [int(arr[i]), i, -1]

    stack = [arr[0]]
    for i in range(1, n):
        value, index = arr[i][0], arr[i][1]

        while stack and value < stack[-1][0]:
            top_stack = stack.pop()
            arr[top_stack[1]][2] = index
        stack.append(arr[i])

    for i in arr:
        print(i[2], end=' ')


if __name__ == '__main__':
    main()
