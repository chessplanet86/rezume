"""
9. Сумма в прямоугольнике
Ограничение времени	3 секунды
Ограничение памяти	256Mb
Ввод	стандартный ввод или input.txt
Вывод	стандартный вывод или output.txt
Вам необходимо ответить на запросы узнать сумму всех элементов числовой матрицы N×M в прямоугольнике с левым верхним углом (x1, y1) и правым нижним (x2, y2)

Формат ввода
В первой строке находится числа N, M размеры матрицы (1 ≤ N, M ≤ 1000) и K — количество запросов (1 ≤ K ≤ 100000). Каждая из следующих N строк содержит по M чисел`— элементы соответствующей строки матрицы (по модулю не превосходят 1000). Последующие K строк содержат по 4 целых числа, разделенных пробелом x1 y1 x2 y2 — запрос на сумму элементов матрице в прямоугольнике (1 ≤ x1 ≤ x2 ≤ N, 1 ≤ y1 ≤ y2 ≤ M)
Формат вывода
Для каждого запроса на отдельной строке выведите его результат — сумму всех чисел в элементов матрице в прямоугольнике (x1, y1), (x2, y2)

Пример
Ввод	Вывод
3 3 2
1 2 3
4 5 6
7 8 9
2 2 3 3
1 1 2 3
28
21
"""


def get_prefix_matrix(arr, count_columns, count_rows):
    b = [[0 for _ in range(count_columns + 1)] for _ in range(count_rows + 1)]
    for i in range(1, count_rows + 1):
        for j in range(1, count_columns + 1):
            b[i][j] = b[i][j - 1] + b[i - 1][j] - b[i - 1][j - 1] + arr[i - 1][j - 1]
    return b


def get_sum_triangle_sector(prefix_matrix, coordinates):
    x0, y0, x1, y1 = coordinates
    rr = (x1 + 1, y1 + 1)
    ll = (x0, y0)
    lr = (x0, y1 + 1)
    rl = (x1 + 1, y0)
    b = prefix_matrix
    return b[rr[0]][rr[1]] + b[ll[0]][ll[1]] - b[rl[0]][rl[1]] - b[lr[0]][lr[1]]


def main():
    a = input().split()
    count_rows, count_columns, count_req = int(a[0]), int(a[1]), int(a[2])
    range_count_rows = range(count_rows)
    range_count_columns = range(count_columns)
    arr = [None for _ in range_count_rows]

    for i in range_count_rows:
        rows = input().split()
        s = [0 for _ in range_count_columns]
        k = 0
        for j in rows:
            s[k] = int(j)
            k += 1

        arr[i] = s

    prefix_matrix = get_prefix_matrix(arr, count_columns, count_rows)

    for _ in range(count_req):
        x0, y0, x1, y1 = [int(i) for i in input().split()]
        coordinates = (x0 - 1, y0 - 1, x1 - 1, y1 - 1)
        print(get_sum_triangle_sector(prefix_matrix, coordinates))


if __name__ == '__main__':
    main()
