import queue


def minEdgeBFS(edges, u, v, n):
    visited = [0] * n

    # Initialize distances as 0
    distance = [0] * n

    # queue to do BFS.
    Q = queue.Queue()
    distance[u] = 0

    Q.put(u)
    visited[u] = True
    parent = {}
    while (not Q.empty()):
        x = Q.get()

        for i in range(len(edges[x])):
            if (visited[edges[x][i]]):
                continue

            distance[edges[x][i]] = distance[x] + 1
            parent[edges[x][i]] = x
            Q.put(edges[x][i])
            visited[edges[x][i]] = 1
    return distance[v] if distance[v] else -1, parent


if __name__ == '__main__':
    n = int(input())
    _range = range(n)
    edges = [[] for i in _range]
    for i in range(n):
        s = [int(x) for x in input().split()]
        for j in _range:
            if s[j]:
                edges[i].append(j)

    u, v = [int(x) - 1 for x in input().split()]
    if u != v:
        text, parent = minEdgeBFS(edges, u, v, n)
        p = v
        path = []
        if text != -1:
            while p != u:
                path.append(str(p+1))
                p = parent[p]
            path.append(str(u+1))
            path.reverse()
            print(len(path) - 1)
            print(' '.join(path))
        else:
            print(text)
    else:
        print(0)
