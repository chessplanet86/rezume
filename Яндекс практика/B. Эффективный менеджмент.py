def main():
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    N, W = [int(x) for x in fp.readline().split()]
    _r = range(N)
    tasks = [None for _ in _r]

    for i in _r:
        line = fp.readline().split()
        tasks[i] = [int(line[0]), int(line[0]) + int(line[1])]

    tasks = sorted(enumerate(tasks), key=lambda x: x[1][0])
    for i in _r:
        tasks[i] = list(tasks[i])

    last_end = -100
    count_tasks = len(tasks)
    k = 0
    arr = []
    arr_append = arr.append
    s = ''
    while count_tasks:
        w = W
        for i in _r:
            if w == 0:
                break
            if tasks[i][1] == None:
                continue
            beg = tasks[i][1][0]
            end = tasks[i][1][1]

            delta = end - beg
            if beg >= last_end:
                if delta <= w:
                    s += (str(tasks[i][0] + 1) + ' ')
                    last_end = end
                    count_tasks -= 1
                    tasks[i][1] = None
                    w = w - delta
        k += 1
        last_end = -100

    fw.write(str(k) + '\n')
    fw.write(s)
    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
