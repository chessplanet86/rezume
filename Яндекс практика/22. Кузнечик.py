def grasshopper(arr, N, k):
    arr[0] = 1
    if k < N:
        for i in range(1, k):
            arr[i] = pow(2, i - 1)

        for i in range(k, N):
            arr[i] = sum(arr[i - k:i])
        return arr[N - 1]
    else:
        for i in range(1, N):
            arr[i] = pow(2, i - 1)
        return arr[N - 1]


def main():
    N, k = [int(x) for x in input().split()]
    arr = [-1 for _ in range(N)]
    print(grasshopper(arr, N, k))


if __name__ == '__main__':
    main()
