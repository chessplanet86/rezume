class Queue:
    def __init__(self, size=100000):
        self.arr = [None for _ in range(size)]
        self.tail = 0
        self.head = 0
        self.max_size = size
        self._size = 0

    def executor(self, fn_name, *args):
        return getattr(self, fn_name)(*args)

    def push(self, *args):
        self.arr[self.head] = args[0]
        self.head = (self.head + 1) % self.max_size
        self._size += 1
        return 'ok'

    def pop(self):
        if not (self.head == self.tail):
            res = self.arr[self.tail % self.max_size]
            self.arr[self.tail % self.max_size] = None
            self.tail = (self.tail + 1) % self.max_size
            self._size -= 1
            return res
        else:
            return 'error'

    def front(self):
        if not (self.head == self.tail):
            return self.arr[self.tail % self.max_size]
        else:
            return 'error'

    def size(self):
        return self._size

    def clear(self):
        for index in range(self.tail, (self.head + 1) % self.max_size):
            self.arr[index] = None
        self.tail = 0
        self.head = 0
        self._size = 0
        return 'ok'

    def exit(self):
        return 'bye'


def main():
    q = Queue(1000)
    fp = open('input.txt', 'r')
    fw = open('output.txt', 'w')
    command = fp.readline()

    while command:
        res = q.executor(*command.split())
        fw.write(str(res) + '\n')
        command = fp.readline()
        if res == 'bye':
            break

    fp.close()
    fw.close()


if __name__ == '__main__':
    main()
