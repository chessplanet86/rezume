def main():
    points = int(input())
    x_min = 1000000000000000000
    y_min = 1000000000000000000
    x_max = -100000000000000000
    y_max = -100000000000000000
    for _ in range(points):
        x, y = [int(j) for j in input().split()]
        if x >= x_max:
            x_max = x
        if x <= x_min:
            x_min = x
        if y >= y_max:
            y_max = y
        if y <= y_min:
            y_min = y

    print(x_min, y_min, x_max, y_max)


if __name__ == '__main__':
    main()
