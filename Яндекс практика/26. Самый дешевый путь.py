def main():
    n, m = [int(x) for x in input().split()]
    arr = [[None for _ in range(m)] for _ in range(n)]
    for i in range(n):
        row = [int(x) for x in input().split()]
        for j in range(m):
            arr[i][j] = row[j]

    w = [[float('inf') for _ in range(m + 1)] for _ in range(n + 1)]
    w[0][0] = 0
    w[0][1] = 0
    w[1][0] = 0

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            w[i][j] = min(w[i - 1][j], w[i][j - 1]) + arr[i - 1][j - 1]

    print(w[-1][-1])


if __name__ == '__main__':
    main()
