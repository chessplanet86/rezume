def main():
    n = int(input())
    first = [0 for x in range(n + 1)]
    row = input().split(' ')
    m = int(input())
    second = [0 for x in range(m + 1)]
    row1 = input().split(' ')

    first_idx = []
    second_idx = []

    for i in range(1, n + 1):
        first[i] = int(row[i - 1])

    for i in range(1, m + 1):
        second[i] = int(row1[i - 1])

    matrix = [[0 for _ in range(n + 1)] for _ in range(m + 1)]

    min_len = min(len(first), len(second))
    for i in range(1, m + 1):
        for j in range(1, n + 1):

            if second[i] == first[j]:
                delta = matrix[i - 1][j - 1] + 1
            else:
                delta = maximum = max(matrix[i][j - 1], matrix[i - 1][j])

            matrix[i][j] = delta

    y = m
    x = n
    k = matrix[y][x]
    first_idx = [0 for _ in range(k)]
    second_idx = [0 for _ in range(k)]
    # print(matrix[y][x])
    while matrix[y][x]:
        if first[x] == second[y]:
            first_idx[k - 1] = str(x)
            second_idx[k - 1] = str(y)
            k -= 1
            x = x - 1
            y = y - 1
        else:
            if matrix[y - 1][x] > matrix[y][x - 1]:
                y = y - 1
            elif matrix[y][x - 1] > matrix[y - 1][x]:
                x = x - 1
            elif second[y] == first[x - 1]:
                x = x - 1
            else:
                y = y - 1

    # print(' '.join(first_idx))
    # print(' '.join(second_idx))

    for i in first_idx:
        print(first[int(i)], end=' ')


if __name__ == '__main__':
    main()
