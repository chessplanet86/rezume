def main():
    n = int(input())
    res = [str(n)]
    if n == 1:
        print(0)
        print(1)
        return
    elif n == 2:
        print(1)
        print(1, 2)
        return
    elif n == 3:
        print(1)
        print(1, 3)
        return

    arr = [i + 1 for i in range(n)]
    last = [0 for _ in range(n)]
    last[1] = 1
    last[2] = 1

    for i in range(3, n):
        x = last[arr[i] // 3 - 1] if arr[i] % 3 == 0 else n + 1
        y = last[arr[i] // 2 - 1] if arr[i] % 2 == 0 else n + 1
        z = last[arr[i] - 1 - 1]
        last[i] = min(x, y, z) + 1

    k = 1
    while n - 1 != 1:
        if n in (2, 3):
            break
        k += 1
        operations = last[n - 1]
        if not arr[n - 1] % 3 and operations - last[arr[n - 1] // 3 - 1] == 1:
            n = arr[arr[n - 1] // 3 - 1]
        elif not arr[n - 1] % 2 and operations - last[arr[n - 1] // 2 - 1] == 1:
            n = arr[arr[n - 1] // 2 - 1]
        elif operations - last[arr[n - 1] - 1 - 1] == 1:
            n = arr[arr[n - 1] - 1 - 1]

        res.append(str(n))

    res.append(str(1))
    res.reverse()
    print(k)
    print(' '.join(res))


if __name__ == '__main__':
    main()
