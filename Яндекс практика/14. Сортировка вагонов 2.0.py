train_length = input()
train = [int(i) for i in input().split()]
counter = 1
stack = []

for car in train:
    if car == counter:
        counter += 1
        while len(stack) != 0 and stack[-1] == counter:
            counter += 1
            stack.pop()
    else:
        stack.append(car)

if len(stack) == 0:
    print('YES')
else:
    print('NO')
