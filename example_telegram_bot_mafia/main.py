import telebot
from telebot import types
import sqlite3
import json
from prettytable import PrettyTable
from io import BytesIO
import os

bot = telebot.TeleBot('5947967411:AAEeg_uyHzqZP3Elu1i-jdZ_46CmF8mTIhU')


def add_game(data: dict):
    con = sqlite3.connect("mafia.db")
    cur = con.cursor()
    filter = str(tuple(data['blacks']))
    for i in range(10):
        cur.execute(f"""
        insert into mafia (player, number_game, best_move, first_short_comp, result_game, discval)
        values ({i + 1}, {data['game']}, 0, 0, 0, 0)
        """)
    if data["result"] == 'black':
        sql = f"""
        update mafia set result_game = 1 where player in {filter}
        and number_game = {data['game']}
        """
        cur.execute(sql)
    elif data["result"] == 'red':
        cur.execute(f"""
        update mafia set result_game = 1 where player not in {filter}
        and number_game = {data['game']}
        """)
    cur.execute(f"""
    update mafia set best_move = {data['best_move'][1]} where player = {data['best_move'][0]}
    and number_game = {data['game']}
    """)
    cur.execute(f"""
    update mafia set first_short_comp = {data['first_short_comp'][1]} where player = {data['first_short_comp'][0]}
    and number_game = {data['game']}
    """)
    con.commit()
    con.close()


def get_result():
    sql = """
            with res as (
            select player
                 , sum(result_game) result
                 , sum(best_move) best_move
                 , sum(first_short_comp* not result_game) first_short_with_result
                 , sum(first_short_comp) all_first_short
                 , sum(discval) discval
            from mafia
            group by player
        )
        , f as (select *
                     , case when all_first_short <= 10
                         then 0.04 * first_short_with_result * all_first_short
                         when all_first_short > 10 then 0.4 * first_short_with_result
                       end short_comp
                from res join players p on p.id = res.player
                order by result + best_move + short_comp + discval desc
        )
        select row_number() over () as n
             , name
             , result
             , best_move
             , short_comp
             , discval
             , result + best_move + short_comp + discval total from f;
    """
    con = sqlite3.connect("mafia.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)

    mytable = PrettyTable()
    mytable.field_names = ["№", "Ник", "Win", "ЛХ", 'ПО', 'discval', 'RES']
    for row in res.fetchall():
        mytable.add_row([r for r in row])
    lines = mytable.get_string(hrules=True)
    try:
        os.remove(os.getcwd(), 'result.txt')
    except:
        pass

    sql = f"""
        select max(number_game) mng from mafia 
        """
    res = cur.execute(sql).fetchone()
    a = res[0] if res[0] else 0

    with open("result.txt", "w") as file:
        file.write(f'Результаты после {a} тура\n')
        file.write(lines)
    con.close()


def get_all_result():
    sql = """
    select m.id, name, number_game game, best_move as bm, result_game as win, discval as dis, first_short_comp as short from mafia m
    join players p on p.id = m.player 
    order by number_game, player
    """
    con = sqlite3.connect("mafia.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    mytable = PrettyTable()
    mytable.field_names = [r[0] for r in res.description]
    for row in res.fetchall():
        mytable.add_row([r for r in row])
    lines = mytable.get_string(hrules=True)
    try:
        os.remove(os.getcwd(), 'all_result.txt')
    except:
        pass
    with open("all_result.txt", "w") as file:
        file.write(lines)
    con.close()


def delete(data: dict):
    con = sqlite3.connect("mafia.db")
    cur = con.cursor()
    sql = f"""
    delete from mafia where number_game = {data['game']}
    """
    cur.execute(sql)
    con.commit()
    con.close()


def truncate():
    con = sqlite3.connect("mafia.db")
    cur = con.cursor()
    sql = f"""
        delete from mafia
        """
    cur.execute(sql)
    con.commit()
    con.close()


def update_rec(data: dict):
    con = sqlite3.connect("mafia.db")
    cur = con.cursor()
    sql = f"""
        update mafia set {data['key']} = {data['value']} where id = {data['id']}
        """
    cur.execute(sql)
    con.commit()
    con.close()


# Функция, обрабатывающая команду /start
@bot.message_handler(commands=["start"])
def start(m, res=False):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Посмотреть результаты участников")
    item2 = types.KeyboardButton("Посмотреть подробные результаты участников")
    item3 = types.KeyboardButton("/add_template")
    item4 = types.KeyboardButton("/del_template")
    item5 = types.KeyboardButton("/update_template")
    markup.add(item1)
    markup.add(item2)
    markup.add(item3)
    markup.add(item4)
    markup.add(item5)
    bot.send_message(m.chat.id, 'Участники: \n 1 - Мама Моши \n 2 - Ермак \n 3 - Поварец \n 4 - Алеха'
                                '\n 5 - Баланс  \n 6 - Собр \n 7 - Груша \n 8 - Гидеон \n 9 - КЩ \n 10 - Китаец',
                     reply_markup=markup)


@bot.message_handler(commands=["help"])
def help(m, res=False):
    string = """
    Доступные команды
    /start
    /add_template
    /del_template
    /update_template
    Бот ожидает сообщения в следующем виде:
        {
           "game": int,
           "command": str,
           "blacks": list[int],
           "best_move": list[int],
           "result": str,
           "first_short_comp": list[int],
           "password": int
        }
    blacks - список черных игроковдлины 3
    
    command in ("add", "update", "delete") is True
    
    result in ("black", "red") is True
    
    best_move список длины 2, где на первом месте первоубиенный, на втором компенсация
    
    first_short_comp список длины 2, где на первом месте первоубиенный, на втором 1 или 0 убили или нет
    
    Пример:
         {
            "game": 1,
            "command": "add",
            "blacks": [5,6,7],
            "best_move": [10, 0.25],
            "result": "black",
            "first_short_comp": [10, 1],
            "password": 12345
         }
    """
    bot.send_message(m.chat.id, string)


@bot.message_handler(commands=["add_template"])
def add_template(m, res=False):
    con = sqlite3.connect("mafia.db")
    cur = con.cursor()
    sql = f"""
    select max(number_game) mng from mafia 
    """
    res = cur.execute(sql).fetchone()
    a = res[0] if res[0] else 0
    con.close()
    tmp = f"""
    {{
       "game": {a + 1},
       "command": "add",
       "blacks": [0,0,0],
       "best_move": [0,0],
       "result": "black red",
       "first_short_comp": [0,1],
       "password": 1
    }}
    """
    bot.send_message(m.chat.id, 'Участники: \n 1 - Мама Моши \n 2 - Ермак \n 3 - Поварец \n 4 - Алеха'
                                '\n 5 - Баланс  \n 6 - Собр \n 7 - Груша \n 8 - Гидеон \n 9 - КЩ \n 10 - Китаец')
    bot.send_message(m.chat.id, tmp)


@bot.message_handler(commands=["del_template"])
def del_template(m, res=False):
    tmp = """
    {
        "game": 000,
        "command": "delete",
        "password": 1
    }
    """
    bot.send_message(m.chat.id, tmp)


@bot.message_handler(commands=["update_template"])
def update_template(m, res=False):
    tmp = """
    {
        "id": 000,
        "key": "best_move first_short_comp result_game discval",
        "value": 000,
        "password": 1
    }
    """
    bot.send_message(m.chat.id, tmp)


# Получение сообщений от юзера
@bot.message_handler(content_types=["text"])
def handle_text(message):
    msg = message.text.strip()
    try:
        data = json.loads(msg)
        print(data)
        command = data['command']
        password = data['password']
        if password == 7500:
            if command == 'add':
                add_game(data=data)
                print('Добавление в базу')
                msg = ' Данные добавлены'
            if command == 'update':
                update_rec(data)
                msg = ' Данные обновлены'
            if command == 'delete':
                delete(data)
                msg = ' Данные удалены'
            if command == 'truncate':
                truncate()
                msg = ' Очистка данных произведена успешно'
            bot.send_message(message.chat.id, 'Вы написали: ' + message.text + msg)
        else:
            bot.send_message(message.chat.id, 'Неверный пароль!')
    except:
        bot.send_message(message.chat.id, 'Формат данных должен быть в виде json')

    if message.text.strip() == 'Посмотреть результаты участников':
        answer = 'Будут выведены результаты участников'
        get_result()
        with open('result.txt', 'rb') as tmp:
            obj = BytesIO(tmp.read())
            obj.name = 'result.txt'
            bot.send_document(message.chat.id, obj)
        bot.send_message(message.chat.id, 'Файл отправлен')

    if message.text.strip() == 'Посмотреть подробные результаты участников':
        get_all_result()
        with open('all_result.txt', 'rb') as tmp:
            obj = BytesIO(tmp.read())
            obj.name = 'all_result.txt'
            bot.send_document(message.chat.id, obj)
        bot.send_message(message.chat.id, 'Файл отправлен')


# Запускаем бота
bot.infinity_polling()
