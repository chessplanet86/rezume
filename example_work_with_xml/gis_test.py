import gis_main as gis
def testFilterByWorker(name, trueCount):
    res = gis.filterByWorkerXML(name)
    assert res==trueCount, f'Функция filterByWorker. Ожидается {trueCount}, получено {res}'


def testSumTimeToDate(atr,date,trueTime):
    res = gis.getSumTimeToDate(date)
    assert res==trueTime, f'Функция getSumTimeToDate. Ожидается {trueTime}, получено {res}'


def testFilterByDates(date1, date2, trueCount):
    res = gis.filterByDates(date1, date2)
    assert res==trueCount, f'Функция filterByDates. Ожидается {trueCount}, получено {res}'

if __name__=='__main__':
    gis.genBinary()
    # Выполняем проверку работоспособности фитра по работникам для тестовых данных.
    for arg in [('G.Tkacheva', 0), ('Y.Melikian', 1), ('Test_Filter_Worker', 3)]:
        try:
            print(f'Тестируется на данных {arg}')
            testFilterByWorker(*arg)
            print('successful')
        except Exception as e:
            print(e)


    # Выполняем проверку работоспособности возврата сумарного времени на дату для тестовых данных.
    for arg in [('Test_Date_Time', '07-01-2021', 8471)]:
        try:
            print(f'Тестируется на данных {arg}')
            testSumTimeToDate(*arg)
            print('successful')
        except Exception as e:
            print(e)

    # Выполняем проверку работоспособности фильтра между датами для тестовых данных.
    for arg in [('01-03-2021 00:00:00', '01-08-2021 00:00:00', 3)]:
        try:
            print(f'Тестируется на данных {arg}')
            testFilterByDates(*arg)
            print('successful')
        except Exception as e:
            print(e)



