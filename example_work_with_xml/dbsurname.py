
import sqlite3
# Создаем базу с фамилиями
con = sqlite3.connect('surnames.db')
cur = con.cursor()
cur.execute(
    """
        CREATE TABLE IF NOT EXISTS surnames (
        id INTEGER NOT NULL,
        surname TEXT,
        PRIMARY KEY(id) 
    );
    """
)
FAM = [line.strip() for line in open('famnew.txt', 'r', encoding='utf8')]

for fam in FAM:
    cur.execute("""INSERT INTO surnames (surname) VALUES (?)""", (fam,))
