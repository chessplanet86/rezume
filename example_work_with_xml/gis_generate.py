import random
import sqlite3
import os
from datetime import datetime

if os.path.exists('zap.db'):
    os.remove('zap.db')

# Создаем базу с фамилиями
con = sqlite3.connect('zap.db')
cur = con.cursor()
cur.execute(
    """
        CREATE TABLE IF NOT EXISTS VISITS (
        id INTEGER NOT NULL,
        surname TEXT,
        date_start TEXT,
        time_start TEXT,
        date_end TEXT,
        time_end TEXT,
        PRIMARY KEY(id) 
    );
    """
)

MAX_TIME = 24*3600
FAM = [line.strip() for line in open('famnew.txt', 'r', encoding='utf8')]
IM = [i for i in 'abcdefghijklmnopqrstuvwxyz']





def randomdate(begin, end):
    year = random.randint(begin, end)
    # month = random.randint(1, 12)
    month = random.randint(1, 2) #Временный месяц
    if (month in (1,3,5,7,10,12)):
        # day = random.randint(1,31)
        day = random.randint(1,2) #Временный день
    elif (month in (4,6,9,11)):
        # day = random.randint(1,30)
        day = random.randint(1,2) #Временный день
    else:
        if (year%4 == 0):
            if (year%100==0 and year%400!=0):
                # day = random.randint(1,28)
                day = random.randint(1,2) #Временный день
            else:
                # day = random.randint(1,29)
                day = random.randint(1,2) #Временный день
        else:
            # day = random.randint(1,28)
            day = random.randint(1,2) #Временный день
    time = random.randint(0, MAX_TIME)
    return (year, month, day, time)


def gen_xml(N):
    f = open('gis_source.xml', 'w', encoding='UTF8')
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<people>\n')
    for i in range(N):
        
        stop=False
        while not stop:
            stop = True
            name = random.choice(IM).upper()+'.'+random.choice(FAM)
            date = randomdate(2021, 2021) #Генерируем случайное время
            available_time = MAX_TIME-1-date[3]
            
            date_start = f'{date[2]:02d}-{date[1]:02d}-{date[0]}'
            date_end = f'{date[2]:02d}-{date[1]:02d}-{date[0]}'
            time_start = f'{date[3]//3600:02d}:{date[3]%3600//60:02d}:{date[3]%3600%60:02d}'
            try:
                delta_time = random.randint(0,available_time)
            except:
                stop = False
                continue
            time_end = f'{(date[3]+delta_time)//3600:02d}:{(date[3]+delta_time)%3600//60:02d}:{(date[3]+delta_time)%3600%60:02d}'

            fam_exists = cur.execute("""SELECT * FROM VISITS WHERE surname=(?)""", (name,))
            
            # Часть где мы проверяем, человек в помещении или нет
            for fam in fam_exists:
                date_insert_start = datetime.strptime(f'{date_start} {time_start}', '%d-%m-%Y %H:%M:%S')
                date_insert_end = datetime.strptime(f'{date_end} {time_end}', '%d-%m-%Y %H:%M:%S')

                condition_in_office_1 = datetime.strptime(f'{fam[2]} {fam[3]}', '%d-%m-%Y %H:%M:%S')<=date_insert_start<=datetime.strptime(f'{fam[4]} {fam[5]}', '%d-%m-%Y %H:%M:%S')
                condition_in_office_2 = datetime.strptime(f'{fam[2]} {fam[3]}', '%d-%m-%Y %H:%M:%S')<=date_insert_end<=datetime.strptime(f'{fam[4]} {fam[5]}', '%d-%m-%Y %H:%M:%S')
                if condition_in_office_1 or condition_in_office_2:
                    stop=False
                    # print('Человек в помещении')
                    # print(name)
                    break
            #Если в это время человека не было в помещении, значит делаем вставку
            if stop==True:
                f.write(f'\t<person full_name="{name}">\n')
                f.write(f'\t\t<start>{date_start} {time_start}</start>\n')
                f.write(f'\t\t<end>{date_end} {time_end}</end>\n')
                f.write(f'\t</person>\n')

                # Добавляем запись в БД
                cur.execute("""
                                INSERT INTO VISITS (surname, date_start, time_start, date_end, time_end) VALUES (?, ?, ?, ?, ?)
                            """, (f'{name}', f'{date_start}', f'{time_start}', f'{date_end}', f'{time_end}')
                )   
                    
        
        
    
    f.write('</people>\n')
    f.close()


def gen_fam():
    f = open('fam.txt', 'r', encoding='utf8')
    g = open('famnew.txt', 'w', encoding='utf8')
    for line in f:
        a = line.split('\t')[2].strip().split(',')
        for i in a:
            g.write(i.strip()+'\n')
    f.close()
    g.close()


if __name__=='__main__':
    gen_xml(15)







