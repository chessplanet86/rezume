import re
import pickle
import os
from datetime import datetime

def get_sec(time_str):
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)


def getSumTimeToDate(date):
    sumTime = 0
    with open('people.pickle', 'rb') as f:
        while True:
            try:
                obj = pickle.load(f)
                if obj['date_start'][0:10]==date:
                    time_start = get_sec(obj['date_start'][11:])
                    time_end = get_sec(obj['date_end'][11:])
                    sumTime += time_end-time_start
            except EOFError:
                break
    print(f'На {date} сумарное время посещений составило:')
    print(f'Часов: {sumTime//3600}\nМинут: {sumTime%3600//60}\nСекунд: {sumTime%3600%60}')
    return sumTime


def getSumTimeBetweenDates(date1, date2):
    sumTime = 0
    with open('people.pickle', 'rb') as f:
        while True:
            try:
                obj = pickle.load(f)
                if datetime.strptime(date1, '%d-%m-%Y')<=datetime.strptime(obj['date_start'], '%d-%m-%Y')<=datetime.strptime(date2, '%d-%m-%Y'):
                    time_start = get_sec(obj['date_start'][11:])
                    time_end = get_sec(obj['date_end'][11:])
                    sumTime += time_end-time_start
            except EOFError:
                break

    print(f'Cумарное время посещений между датами {date1} и {date2} составило:')
    print(f'Часов: {sumTime//3600}\nМинут:{sumTime%3600//60}\nСекунд:{sumTime%3600%60}')


def filterByWorkerXML(surname, test=''):
    count = 0
    with open('people.pickle', 'rb') as f:
        with open(f'filter_by_worker_gis{test}.xml', 'w', encoding='utf8') as g:
            g.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            g.write('<people>\n')
            while True:
                try:
                    obj = pickle.load(f)
                    if obj['name']==surname:
                        count+=1
                        g.write(f'\t<person full_name="{obj["name"]}">\n')
                        g.write(f'\t\t<start>{obj["date_start"]}</start>\n')
                        g.write(f'\t\t<end>{obj["date_end"]}</end>\n')
                        g.write(f'\t</person>\n')
                except EOFError:
                    g.write('</people>\n')
                    break
        return count


def filterByDates(date1, date2):
    count = 0
    with open('people.pickle', 'rb') as f:
        with open('filter_by_dates_gis.xml', 'w', encoding='utf8') as g:
            g.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            g.write('<people>\n')
            while True:
                try:
                    obj = pickle.load(f)
                    if datetime.strptime(date1, '%d-%m-%Y %X')<=datetime.strptime(obj['date_start'], '%d-%m-%Y %X')<=datetime.strptime(date2, '%d-%m-%Y %X'):
                        g.write(f'\t<person full_name="{obj["name"]}">\n')
                        g.write(f'\t\t<start>{obj["date_start"]}</start>\n')
                        g.write(f'\t\t<end>{obj["date_end"]}</end>\n')
                        g.write(f'\t</person>\n')
                        count+=1
                except EOFError:
                    g.write('</people>\n')
                    break
    return count


def genBinary():
    if os.path.exists('people.pickle'):
        os.remove('people.pickle')

    f = open('gis_source.xml', 'r', encoding='utf8')
    f.__next__()
    f.__next__()
    k=0 #Счетчик для выбора необходимой функции для парсинга
    def getAtrr(string):
        return re.findall('full_name="(.+)">', string)
    def getDateStart(string):
        return re.findall('<start>(.+)</start>', string)
    def getDateEnd(string):
        return re.findall('<end>(.+)</end>', string)
    def nextTeg(string):
        return [None]

    funcs = [getAtrr, getDateStart, getDateEnd, nextTeg]

    objKeys = ['name', 'date_start', 'date_end', None]

    tmp_obj = {
        'name' : 'tmpName',
        'date_start' : 'tmpDate',
        'date_end' : 'tmp_date',
        None: None
    }
    with open('people.pickle', 'wb') as g:
        try:
            for i in f:
                data = funcs[k](i)[0]
                tmp_obj[objKeys[k]] = data
                k+=1
                if (k==4):
                    pickle.dump(tmp_obj, g)
                k%=4
        except Exception as e:
            pass


if __name__=='__main__':
    genBinary()


    


