import json
import sqlite3
from config import COUNTS_GAME, FINAL_GAMES, COUNT_PASS_GAMES


def get_players():
    sql = """
    select id as id_player
         , row_number() over () as n
         , name
         , slot
         from players
         order by slot;
    """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res


def get_all_players(number_table=None, number_game=None):
    if not number_table:
        sql = """
        select id_player as id_player
             , row_number() over () as n
             , name
             , name_comand
             , club
             from all_players;
        """
    else:
        sql = get_gamers_sql(number_table, number_game)
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    fetch = res.fetchall()
    con.close()
    return fetch


def get_gamers_sql(number_table, number_game):
    sql = f"""
            with res as
            (select s.id_player, slot from seating as s where id_table = {number_table} and id_game = {number_game - 1})
            select s.id_player, a.name, slot from res s join all_players a on s.id_player = a.id_player
            order by slot
            """
    return sql


def get_count_slots(id_player):
    sql = f"""
    with res as (
    select * from seating where id_player = {id_player}
    )
    select id_player , slot, count(slot) as count from res
    group by id_player, slot
    """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    fetch = res.fetchall()
    res = [r for r in fetch]
    con.close()
    return res


def get_value_by_key(options, key):
    for option in options:
        if option.key == int(key):
            return option.text


def init_db():
    sql = """
        delete from players
        """
    con = sqlite3.connect("mini_tournament.db")
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()


def get_not_empty_extra_points(extra_points_refs):
    not_empty_extra_points = dict()
    for key, extra_point in extra_points_refs.items():
        cur = extra_point.current
        if float(cur.value) != 0:
            not_empty_extra_points[key] = {'data': cur.data, 'points': cur.value, 'label': cur.label}
    return not_empty_extra_points


def get_not_empty_discvals(discvals_refs):
    not_empty_extra_points = dict()
    for key, extra_point in discvals_refs.items():
        cur = extra_point.current
        if int(cur.value) != 0:
            not_empty_extra_points[key] = {'data': cur.data, 'discval': cur.value, 'label': cur.label}
    return not_empty_extra_points


def get_max_tour():
    con = sqlite3.connect("mini_tournament.db")
    cur = con.cursor()
    sql = f"""
        select max(number_game) mng from games 
        """
    res = cur.execute(sql).fetchone()
    a = res[0] if res[0] else 1
    con.close()
    return a


def get_seating_finals():
    table_from_base = 'games'
    multiple = 1
    count_games = COUNTS_GAME  # Здесь не именно количество игр, которых сыграл игрок
    multiple_final = 1.3
    count_games_final = FINAL_GAMES  # Здесь не именно количество игр, которых сыграл игрок
    table_from_base_final = 'final_games'

    players = final_comand()
    comands = [(r['id_comand'], r['command_total'], r['id_player']) for r in players]
    commands_dict = {}
    ids_comand_filter = []
    for player in comands:
        commands_dict[player[0]] = 0

    for player in comands:
        commands_dict[player[0]] += player[1]

    for key in commands_dict:
        ids_comand_filter.append((key, commands_dict[key]))

    ids_comand_filter.sort(key=lambda x: -x[1])
    filter = []
    for i in range(10):
        filter.append(ids_comand_filter[i][0])
    comands_filter = str(tuple(filter))

    sql = f"""
        with res as (
                select id_player
                     , sum(result_game * {multiple}) result
                     , sum(best_move * {multiple}) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points * {multiple}) discval
                     , sum(extra_points * {multiple}) extra_points

                from {table_from_base}
                group by id_player
            )
        , koef_dops as (select round(0.4*{count_games}) midle, 0.4 / round(0.4*{count_games}) koef from {table_from_base})
        , f as (select *
                     , case when all_first_short <= (select midle from koef_dops)
                         then (select koef from koef_dops) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                       end short_comp
                from res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        , qualification_games as (select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp * {multiple}, 3), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp * {multiple},0) + discval + round(extra_points, 2), 2) total 
             from f)
        , res_final as (
                select id_player
                     , sum(result_game * {multiple_final}) result
                     , sum(best_move * {multiple_final}) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points * {multiple_final}) discval
                     , sum(extra_points * {multiple_final}) extra_points

                from {table_from_base_final}
                group by id_player
            )
        , koef_dops_final as (select round(0.4*{count_games_final}) midle, 0.4 / round(0.4*{count_games_final}) koef from {table_from_base_final})
        , f_final as (select *
                     , case when all_first_short <= (select midle from koef_dops_final)
                         then (select koef from koef_dops_final) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops_final) then 0.4 * first_short_with_result
                       end short_comp
                from res_final as res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        , final as (select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp * {multiple}, 3), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp * {multiple},0) + discval + round(extra_points, 2), 2) total 
             from f_final as f)
        , itog as (select q.name, q.id_player, q.id_comand, q.total + coalesce(ff.total,0) as total from qualification_games q left join final ff on q.id_player = ff.id_player)
        select id_player from itog where id_comand in {comands_filter} order by id_comand, total desc 
        """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res


def get_selections_or_finals_result(count_games=COUNTS_GAME, order_str=None, table_from_base='games'):
    if order_str:
        order = 'id_comand,'
    else:
        order = ''

    if table_from_base == 'final_games':
        multiple = 1.3
    else:
        multiple = 1

    order_str = f'order by {order} round(result + best_move + coalesce(short_comp,0) + discval + round(extra_points, 2), 2) desc'
    sql = f"""
    with res as (
            select id_player
                 , sum(result_game * {multiple}) result
                 , sum(best_move * {multiple}) best_move
                 , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                 , sum(first_short) all_first_short
                 , sum(discval_points * {multiple}) discval
                 , sum(extra_points * {multiple}) extra_points
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then 1 else 0 end) black_count_games
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then result_game * {multiple} else 0 end) black_win_games
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then discval_points * {multiple} else 0 end) black_discvals
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then extra_points * {multiple} else 0 end) black_extra
                 , sum(case when kommisar=1 then 1 else 0 end) kommisar_count_games
                 , sum(case when kommisar=1 then result_game * {multiple} else 0 end) kommisar_win_games
                 , sum(case when kommisar=1 then discval_points * {multiple} else 0 end) kommisar_discvals
                 , sum(case when kommisar=1 then best_move * {multiple} else 0 end) kommisar_best_move
                 , sum(case when kommisar=1 then extra_points * {multiple} else 0 end) kommisar_extra
                 , sum(case when pistolet_1=1 or pistolet_2=1 then 1 else 0 end) pistolet_count_games
                 , sum(case when pistolet_1=1 or pistolet_2=1 then result_game * {multiple} else 0 end) pistolet_win_games
                 , sum(case when pistolet_1=1 or pistolet_2=1 then discval_points * {multiple} else 0 end) pistolet_discvals
                 , sum(case when pistolet_1=1 or pistolet_2=1 then extra_points * {multiple} else 0 end) pistolet_extra
                 , sum(case when red=1 then 1 else 0 end) red_count_games
                 , sum(case when red=1 then result_game * {multiple} else 0 end) red_win_games
                 , sum(case when red=1 then discval_points * {multiple} else 0 end) red_discvals
                 , sum(case when red=1 then extra_points * {multiple} else 0 end) red_extra
                 , sum(case when red=1 then best_move * {multiple} else 0 end) red_best_move
                 , sum(case when don=1 then 1 else 0 end) don_count_games
                 , sum(case when don=1 then result_game * {multiple} else 0 end) don_win_games
                 , sum(case when don=1 then discval_points * {multiple} else 0 end) don_discvals
                 , sum(case when don=1 then extra_points * {multiple} else 0 end) don_extra
            from {table_from_base}
            group by id_player
        )
    , koef_dops as (select round(0.4*{count_games}) midle, 0.4 / round(0.4*{count_games}) koef from {table_from_base})
    , f as (select *
                 , case when all_first_short <= (select midle from koef_dops)
                     then (select koef from koef_dops) * first_short_with_result * all_first_short
                     -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                     when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                   end short_comp
            from res join all_players p on p.id_player = res.id_player
            --order by result + best_move + coalesce(short_comp,0) + discval desc
    )
    select row_number() over () as n
         , f.name
         , result
         , f.id_player
         , f.id_comand
         , round(best_move, 2) best_move
         , coalesce(round(short_comp * {multiple}, 3), 0) short_comp
         , discval
         , all_first_short
         , first_short_with_result
         , round(extra_points, 3) extra_points
         , round(result + best_move + coalesce(short_comp * {multiple},0) + discval + round(extra_points, 3), 3) total 
         , black_count_games
         , black_win_games
         , black_discvals
         , black_extra
         , kommisar_count_games
         , kommisar_win_games
         , kommisar_discvals
         , kommisar_best_move
         , kommisar_extra
         , pistolet_count_games
         , pistolet_win_games
         , pistolet_discvals
         , pistolet_extra
         , red_count_games
         , red_win_games
         , red_discvals
         , red_extra
         , red_best_move
         , don_count_games
         , don_win_games
         , don_discvals
         , don_extra 
         from f
    {order_str}
    """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res


def get_tournament_results():
    multiple_selections = 1
    multiple = 1.3
    table_selections = 'games'
    table_finals = 'final_games'
    count_selections = COUNTS_GAME
    count_finals = FINAL_GAMES
    sql = f"""
        with res_selections as (
                select id_player
                     , sum(result_game * {multiple_selections}) result
                     , sum(best_move * {multiple_selections}) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points * {multiple_selections}) discval
                     , sum(extra_points * {multiple_selections}) extra_points
                     -----Для статистики----
                     , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then 1 else 0 end) black_count_games
                     , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then result_game * {multiple_selections} else 0 end) black_win_games
                     , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then discval_points * {multiple_selections} else 0 end) black_discvals
                     , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then extra_points * {multiple_selections} else 0 end) black_extra
                     , sum(case when kommisar=1 then 1 else 0 end) kommisar_count_games
                     , sum(case when kommisar=1 then result_game * {multiple_selections} else 0 end) kommisar_win_games
                     , sum(case when kommisar=1 then discval_points * {multiple_selections} else 0 end) kommisar_discvals
                     , sum(case when kommisar=1 then best_move * {multiple_selections} else 0 end) kommisar_best_move
                     , sum(case when kommisar=1 then extra_points * {multiple_selections} else 0 end) kommisar_extra
                     , sum(case when pistolet_1=1 or pistolet_2=1 then 1 else 0 end) pistolet_count_games
                     , sum(case when pistolet_1=1 or pistolet_2=1 then result_game * {multiple_selections} else 0 end) pistolet_win_games
                     , sum(case when pistolet_1=1 or pistolet_2=1 then discval_points * {multiple_selections} else 0 end) pistolet_discvals
                     , sum(case when pistolet_1=1 or pistolet_2=1 then extra_points * {multiple_selections} else 0 end) pistolet_extra
                     , sum(case when red=1 then 1 else 0 end) red_count_games
                     , sum(case when red=1 then result_game * {multiple_selections} else 0 end) red_win_games
                     , sum(case when red=1 then discval_points * {multiple_selections} else 0 end) red_discvals
                     , sum(case when red=1 then extra_points * {multiple_selections} else 0 end) red_extra
                     , sum(case when red=1 then best_move * {multiple_selections} else 0 end) red_best_move
                     , sum(case when don=1 then 1 else 0 end) don_count_games
                     , sum(case when don=1 then result_game * {multiple_selections} else 0 end) don_win_games
                     , sum(case when don=1 then discval_points * {multiple_selections} else 0 end) don_discvals
                     , sum(case when don=1 then extra_points * {multiple_selections} else 0 end) don_extra
                from {table_selections}
                group by id_player
            )
        , koef_dops_selections as (select round(0.4*{count_selections}) midle, 0.4 / round(0.4*{count_selections}) koef from {table_selections})
        , f_selections as (select *
                     , case when all_first_short <= (select midle from koef_dops_selections)
                         then (select koef from koef_dops_selections) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops_selections) then 0.4 * first_short_with_result
                       end short_comp
                from res_selections res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        , selections as (select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp * {multiple_selections}, 3), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp * {multiple_selections},0) + discval + round(extra_points, 2), 2) total
             ----Для статистики----
             , black_count_games
             , black_win_games
             , black_discvals
             , black_extra
             , kommisar_count_games
             , kommisar_win_games
             , kommisar_discvals
             , kommisar_best_move
             , kommisar_extra
             , pistolet_count_games
             , pistolet_win_games
             , pistolet_discvals
             , pistolet_extra
             , red_count_games
             , red_win_games
             , red_discvals
             , red_extra
             , red_best_move
             , don_count_games
             , don_win_games
             , don_discvals
             , don_extra
             from f_selections f)
        , res_finals as (
            select id_player
                 , sum(result_game * {multiple}) result
                 , sum(best_move * {multiple}) best_move
                 , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                 , sum(first_short) all_first_short
                 , sum(discval_points * {multiple}) discval
                 , sum(extra_points * {multiple}) extra_points
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then 1 else 0 end) black_count_games
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then result_game * {multiple} else 0 end) black_win_games
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then discval_points * {multiple} else 0 end) black_discvals
                 , sum(case when don=1 or pistolet_1=1 or pistolet_2=1 then extra_points * {multiple} else 0 end) black_extra
                 , sum(case when kommisar=1 then 1 else 0 end) kommisar_count_games
                 , sum(case when kommisar=1 then result_game * {multiple} else 0 end) kommisar_win_games
                 , sum(case when kommisar=1 then discval_points * {multiple} else 0 end) kommisar_discvals
                 , sum(case when kommisar=1 then best_move * {multiple} else 0 end) kommisar_best_move
                 , sum(case when kommisar=1 then extra_points * {multiple} else 0 end) kommisar_extra
                 , sum(case when pistolet_1=1 or pistolet_2=1 then 1 else 0 end) pistolet_count_games
                 , sum(case when pistolet_1=1 or pistolet_2=1 then result_game * {multiple} else 0 end) pistolet_win_games
                 , sum(case when pistolet_1=1 or pistolet_2=1 then discval_points * {multiple} else 0 end) pistolet_discvals
                 , sum(case when pistolet_1=1 or pistolet_2=1 then extra_points * {multiple} else 0 end) pistolet_extra
                 , sum(case when red=1 then 1 else 0 end) red_count_games
                 , sum(case when red=1 then result_game * {multiple} else 0 end) red_win_games
                 , sum(case when red=1 then discval_points * {multiple} else 0 end) red_discvals
                 , sum(case when red=1 then extra_points * {multiple} else 0 end) red_extra
                 , sum(case when red=1 then best_move * {multiple} else 0 end) red_best_move
                 , sum(case when don=1 then 1 else 0 end) don_count_games
                 , sum(case when don=1 then result_game * {multiple} else 0 end) don_win_games
                 , sum(case when don=1 then discval_points * {multiple} else 0 end) don_discvals
                 , sum(case when don=1 then extra_points * {multiple} else 0 end) don_extra
            from {table_finals}
            group by id_player
        )
    , koef_dops_finals as (select round(0.4*{count_finals}) midle, 0.4 / round(0.4*{count_finals}) koef from {table_finals})
    , f_finals as (select *
                 , case when all_first_short <= (select midle from koef_dops_finals)
                     then (select koef from koef_dops_finals) * first_short_with_result * all_first_short
                     -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                     when all_first_short > (select midle from koef_dops_finals) then 0.4 * first_short_with_result
                   end short_comp
            from res_finals as res join all_players p on p.id_player = res.id_player
            --order by result + best_move + coalesce(short_comp,0) + discval desc
    )
    , finals as (select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp * {multiple}, 3), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp * {multiple},0) + discval + round(extra_points, 2), 2) total
             ----Для статистики----
             , black_count_games
             , black_win_games
             , black_discvals
             , black_extra
             , kommisar_count_games
             , kommisar_win_games
             , kommisar_discvals
             , kommisar_best_move
             , kommisar_extra
             , pistolet_count_games
             , pistolet_win_games
             , pistolet_discvals
             , pistolet_extra
             , red_count_games
             , red_win_games
             , red_discvals
             , red_extra
             , red_best_move
             , don_count_games
             , don_win_games
             , don_discvals
             , don_extra
             from f_finals f)
    select s.id_player
         , s.id_comand
         , s.name
         , s.result + coalesce(f.result, 0) as result
         , s.best_move + coalesce(f.best_move, 0) as best_move
         , s.short_comp + coalesce(f.short_comp, 0) as short_comp
         , s.discval + coalesce(f.discval, 0) as discval
         , s.all_first_short + coalesce(f.all_first_short, 0) as all_first_short
         , s.first_short_with_result + coalesce(f.first_short_with_result, 0) as first_short_with_result
         , s.extra_points + coalesce(f.extra_points, 0) as extra_points
         , round(s.total + coalesce(f.total, 0), 2) as total
         , s.black_count_games + coalesce(f.black_count_games, 0) as black_count_games
         , s.black_win_games + coalesce(f.black_win_games, 0) as black_win_games
         , s.black_discvals + coalesce(f.black_discvals, 0) as black_discvals
         , s.black_extra + coalesce(f.black_extra, 0) as black_extra
         , s.kommisar_count_games + coalesce(f.kommisar_count_games, 0) as kommisar_count_games
         , s.kommisar_win_games + coalesce(f.kommisar_win_games, 0) as kommisar_win_games
         , s.kommisar_discvals + coalesce(f.kommisar_discvals, 0) as kommisar_discvals
         , s.kommisar_best_move + coalesce(f.kommisar_best_move, 0) as kommisar_best_move
         , s.kommisar_extra + coalesce(f.kommisar_extra, 0) as kommisar_extra
         , s.pistolet_count_games + coalesce(f.pistolet_count_games, 0) as pistolet_count_games
         , s.pistolet_win_games + coalesce(f.pistolet_win_games,0) as pistolet_win_games
         , s.pistolet_discvals + coalesce(f.pistolet_discvals, 0) as pistolet_discvals
         , s.pistolet_extra + coalesce(f.pistolet_extra, 0) as pistolet_extra
         , s.red_count_games + coalesce(f.red_count_games, 0) as red_count_games
         , s.red_win_games + coalesce(f.red_win_games, 0) as red_win_games
         , s.red_discvals + coalesce(f.red_discvals, 0) as red_discvals
         , s.red_extra + coalesce(f.red_extra, 0) as red_extra
         , s.red_best_move + coalesce(f.red_best_move, 0) as red_best_move
         , s.don_count_games + coalesce(f.don_count_games, 0) as don_count_games
         , s.don_win_games + coalesce(f.don_win_games, 0) as don_win_games
         , s.don_discvals + coalesce(f.don_discvals, 0) as don_discvals
         , s.don_extra + coalesce(f.don_extra, 0) as don_extra
    from selections s left join finals f on f.id_player = s.id_player
    order by round(s.total + coalesce(f.total, 0), 2) desc
    """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res


def final_draw(selection_table_games_from_base='games', count_selection_games=COUNTS_GAME,
               final_table_games_from_base='final_games', final_count_games=FINAL_GAMES):
    sql = f"""
        with selection_res as (
        with res as (
                select id_player
                     , sum(result_game) result
                     , sum(best_move) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points) discval
                     , sum(extra_points) extra_points
                from {selection_table_games_from_base}
                group by id_player
            )
        , koef_dops as (select round(0.4*{count_selection_games}) midle, 0.4 / round(0.4*{count_selection_games}) koef from {selection_table_games_from_base})
        , f as (select *
                     , case when all_first_short <= (select midle from koef_dops)
                         then (select koef from koef_dops) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                       end short_comp
                from res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp, 2), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp,0) + discval + round(extra_points, 2), 2) total from f)
        , final_res as (
        with res as (
                select id_player
                     , sum(result_game) result
                     , sum(best_move) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points) discval
                     , sum(extra_points) extra_points
                from {final_table_games_from_base}
                group by id_player
            )
        , koef_dops as (select round(0.4*{final_count_games}) midle, 0.4 / round(0.4*{final_count_games}) koef from {final_table_games_from_base})
        , f as (select *
                     , case when all_first_short <= (select midle from koef_dops)
                         then (select koef from koef_dops) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                       end short_comp
                from res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp, 2), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp,0) + discval + round(extra_points, 2), 2) total from f)
        select t1.id_player, t1.id_comand, round(t1.total + t2.total,2) as command_total, a.name_comand
        from selection_res t1
        join final_res t2 on t2.id_player = t1.id_player
        join all_players a on a.id_player = t1.id_player
        order by t1.id_comand, t1.total + t2.total desc 
        """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res

def final_comand(selection_table_games_from_base='games', count_selection_games=COUNTS_GAME,
               final_table_games_from_base='final_games', final_count_games=FINAL_GAMES):

    multiple = 1.3
    sql = f"""
        with selection_res as (
        with res as (
                select id_player
                     , sum(result_game) result
                     , sum(best_move) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points) discval
                     , sum(extra_points) extra_points
                from {selection_table_games_from_base}
                group by id_player
            )
        , koef_dops as (select round(0.4*{count_selection_games}) midle, 0.4 / round(0.4*{count_selection_games}) koef from {selection_table_games_from_base})
        , f as (select *
                     , case when all_first_short <= (select midle from koef_dops)
                         then (select koef from koef_dops) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                       end short_comp
                from res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp, 2), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp,0) + discval + round(extra_points, 2), 2) total from f)
        , final_res as (
        with res as (
                select id_player
                     , sum(result_game * {multiple}) result
                     , sum(best_move * {multiple}) best_move
                     , sum(first_short * case when not result_game then 1 else 0.5 end * has_compens) first_short_with_result
                     , sum(first_short) all_first_short
                     , sum(discval_points * {multiple}) discval
                     , sum(extra_points * {multiple}) extra_points
                from {final_table_games_from_base}
                group by id_player
            )
        , koef_dops as (select round(0.4*{final_count_games}) midle, 0.4 / round(0.4*{final_count_games}) koef from {final_table_games_from_base})
        , f as (select *
                     , case when all_first_short <= (select midle from koef_dops)
                         then (select koef from koef_dops) * first_short_with_result * all_first_short
                         -- n = число игр * 0.4 округленное до целого. для 8 игр получаем 3
                         when all_first_short > (select midle from koef_dops) then 0.4 * first_short_with_result
                       end short_comp
                from res join all_players p on p.id_player = res.id_player
                --order by result + best_move + coalesce(short_comp,0) + discval desc
        )
        select row_number() over () as n
             , f.name
             , result
             , f.id_player
             , f.id_comand
             , round(best_move, 2) best_move
             , coalesce(round(short_comp * {multiple}, 2), 0) short_comp
             , discval
             , all_first_short
             , first_short_with_result
             , round(extra_points, 2) extra_points
             , round(result + best_move + coalesce(short_comp,0) + discval + round(extra_points, 2), 2) total from f)
        select t1.id_player, t1.id_comand, round(t1.total + coalesce(t2.total, 0),2) as command_total, a.name_comand
        from selection_res t1
        left join final_res t2 on t2.id_player = t1.id_player
        join all_players a on a.id_player = t1.id_player
        order by t1.id_comand, t1.total + t2.total desc 
        """
    con = sqlite3.connect("mini_tournament.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute(sql)
    res = [r for r in res.fetchall()]
    con.close()
    return res

def save_game_result(result_game):
    con = sqlite3.connect("mini_tournament.db")
    cur = con.cursor()
    sql = f"""
    select max(number_game) mng from games 
    """
    res = cur.execute(sql).fetchone()
    a = res[0] + 1 if res[0] else 1
    don = int(result_game['don'].value)
    pistolet_1 = int(result_game['pistolet_1'].value)
    pistolet_2 = int(result_game['pistolet_2'].value)
    commisar = int(result_game['commisar'].value)
    filter = str((don, pistolet_1, pistolet_2))
    players = get_players()

    number_game = result_game['number_game']
    number_table = result_game['number_table']
    name_table = 'games' if number_game <= (COUNTS_GAME + COUNT_PASS_GAMES) else 'final_games'
    sql = f"""
    select * from {name_table} where number_game={number_game} and number_table = {number_table}
    """
    is_merge = cur.execute(sql)
    if not is_merge.fetchone():
        for i in range(10):
            cur.execute(f"""
            insert into {name_table} (id_player, number_game, number_table, best_move, first_short, extra_points, discval_points, result_game, kommisar, don)
            values ({players[i]['id_player']}, {number_game}, {number_table}, 0, 0, 0, 0, 0, 0, 0)
            """)
    else:
        cur.execute(f"""delete from {name_table} where number_game={number_game} and number_table = {number_table}""")
        for i in range(10):
            cur.execute(f"""
            insert into {name_table} (id_player, number_game, number_table, best_move, first_short, extra_points, discval_points, result_game, kommisar, don)
            values ({players[i]['id_player']}, {number_game}, {number_table}, 0, 0, 0, 0, 0, 0, 0)
            """)

    cur.execute(f"""
    update {name_table} set don = 1 where id_player = {don} and number_game={number_game} and number_table = {number_table}
    """)

    cur.execute(f"""
    update {name_table} set pistolet_1 = 1 where id_player = {pistolet_1} and number_game={number_game} and number_table = {number_table}
    """)

    cur.execute(f"""
    update {name_table} set pistolet_2 = 1 where id_player = {pistolet_2} and number_game={number_game} and number_table = {number_table}
    """)

    cur.execute(f"""
    update {name_table} set kommisar = 1 where id_player = {commisar} and number_game={number_game} and number_table = {number_table}
    """)

    cur.execute(f"""
    update {name_table} set red = 1 where id_player not in ({commisar}, {pistolet_2}, {pistolet_1}, {don}) and number_game={number_game} and number_table = {number_table}
    """)

    if result_game['winner'] == 'black':
        cur.execute(f"""
        update {name_table} set result_game = 1 where id_player in {filter} and number_game={number_game} and number_table = {number_table}
        """)
    elif result_game['winner'] == 'red':
        cur.execute(f"""
        update {name_table} set result_game = 1 where id_player not in {filter} and number_game={number_game} and number_table = {number_table}
        """)

    cur.execute(f"""
    update {name_table} set first_short = 1 where id_player = {int(result_game['first_dead'].value)}
    and number_game={number_game} and number_table = {number_table} and id_player not in {filter}
    """)

    if result_game['best_move'].value in (0, '0'):
        has_compens = 0
    else:
        has_compens = 1
        if result_game['best_move'].value == 'Получает компенсацию':
            result_game['best_move'].value = 0
        else:
            pass

    cur.execute(f"""
    update {name_table} set best_move = {float(result_game['best_move'].value)}, has_compens = {has_compens}
    where id_player = {int(result_game['first_dead'].value)} and id_player not in {filter}
    and number_game={number_game} and number_table = {number_table}
    """)

    recs = json.loads(result_game['extra_points'])
    for key in recs:
        value = int(recs[key]['points']) / 10
        id_player = recs[key]['data']['id_player']
        cur.execute(f"""
        update {name_table} set extra_points = {value} where id_player = {id_player} and number_game={number_game} and number_table = {number_table}
        """)

    recs = json.loads(result_game['discvals'])
    for key in recs:
        value = -0.5
        id_player = recs[key]['data']['id_player']
        cur.execute(f"""
        update {name_table} set discval_points = {value} where id_player = {id_player} and number_game={number_game} and number_table = {number_table}
        """)

    con.commit()
    con.close()
