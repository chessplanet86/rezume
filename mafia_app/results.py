from config import COUNTS_GAME, FINAL_GAMES, COUNT_PASS_GAMES
import sqlite3
import flet as ft


class ResultsTable:
    def __init__(self, page, number_table=None, number_game=None, table_from_base='games'):
        self.ref = ft.Ref[ft.DataTable]()
        self.page = page
        self.chose_cell_event = None
        self.table = ft.DataTable(
            data={},
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=430,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("№"), numeric=True, tooltip="Игровой номер"),
                ft.DataColumn(ft.Text(f"Игра {number_game}"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text(f"Роль"), tooltip="Роль"),
                ft.DataColumn(ft.Text(f"Баллы"), tooltip="Баллы"),
            ],
            rows=self.get_rows(number_table, number_game, table_from_base)
        )
        # page.update()

    def get_rows(self, number_table=None, number_game=None, table_from_base=None):
        results_players = self.get_results_players(number_table, number_game, table_from_base)
        rows_players = []
        n = 0
        for p in results_players:
            n += 1
            role = ft.Text('')

            if p['kommisar']:
                role = ft.Row([ft.Icon(name=ft.icons.STAR_RATE_OUTLINED, color=ft.colors.YELLOW)])
            if p['don']:
                role = ft.Row([ft.Icon(name=ft.icons.FACE_SHARP, color=ft.colors.BLACK)])
            if p['pistolet_1'] or p['pistolet_2']:
                role = ft.Row([ft.Icon(name=ft.icons.FACE_OUTLINED, color=ft.colors.BLACK)])

            if p['first_short']:
                if isinstance(role, ft.Row):
                    role.controls.append(ft.Icon(name=ft.icons.GPS_FIXED, color=ft.colors.RED))
                else:
                    role = ft.Row([ft.Icon(name=ft.icons.GPS_FIXED, color=ft.colors.RED)])
            totals = p['totals']

            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(n)),
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(role),
                    ft.DataCell(ft.Text(totals)),
                ],
                selected=False
            )
            rows_players.append(dataRow)
        return rows_players

    def get_results_players(self, number_table, number_game, table='games'):
        if table == 'final_games':
            view = 'v_compens_4_final'
            multiple = 1.3
        else:
            view = 'v_compens_8'
            multiple = 1

        sql = f"""
            select g.id_player as id_player
             , row_number() over () as n
             , a.name
             , first_short
             , kommisar
             , don
             , pistolet_1
             , pistolet_2
             , extra_points
             , best_move
             , discval_points
             , result_game
             , round(result_game * {multiple} + extra_points * {multiple} + discval_points * {multiple} + best_move * {multiple} + coalesce((case when result_game = 0 then 2 else 1 end)*(short_comp * has_compens)/proporc_compens, 0), 3) as totals
             , has_compens
             --, slot
             from {table} g join all_players a on a.id_player = g.id_player
             and g.number_game = {number_game} and g.number_table = {number_table}
             join {view} vc on vc.id_player = a.id_player
        """
        con = sqlite3.connect("mini_tournament.db")
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        res = cur.execute(sql)
        res = [r for r in res.fetchall()]
        con.close()
        return res


def container_results(page):
    lv_left = ft.ListView(expand=True, spacing=10, padding=20)
    lv_right = ft.ListView(expand=True, spacing=10, padding=20)
    row_1 = [
        ft.Row([ft.Text(f"Результаты 1 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)]),
    ]

    row_2 = [
        ft.Row([ft.Text(f"Результаты 2 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)])
    ]

    step = 2
    for number_game in range(1, COUNTS_GAME + COUNT_PASS_GAMES + 1, step):
        tables = []
        for i in range(step):
            table = ResultsTable(page, 1, number_game + i, 'games')
            t = table.table
            tables.append(t)
        row_1.append(
            ft.Row(tables, wrap=True)
        )
        tables = []
        for i in range(step):
            table = ResultsTable(page, 2, number_game + i, 'games')
            t = table.table
            tables.append(t)
        row_2.append(
            ft.Row(tables, wrap=True)
        )

    left_grid = ft.Column(row_1)
    right_grid = ft.Column(row_2)
    lv_left.controls.append(left_grid)
    lv_right.controls.append(right_grid)
    row = ft.Row(
        [
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_left
            ),
            ft.VerticalDivider(),
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_right
            )
        ],
        spacing=0,
        expand=True,
        auto_scroll=True

    )

    cont_seating = ft.Container(
        content=ft.Row([row], expand=True)
    )
    return cont_seating


def container_results_final(page):
    lv_left = ft.ListView(expand=True, spacing=10, padding=20)
    lv_right = ft.ListView(expand=True, spacing=10, padding=20)
    row_1 = [
        ft.Row([ft.Text(f"Результаты 1 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)]),
    ]

    row_2 = [
        ft.Row([ft.Text(f"Результаты 2 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)])
    ]

    step = 2
    for number_game in range(COUNTS_GAME + COUNT_PASS_GAMES + 1, COUNTS_GAME + COUNT_PASS_GAMES + 1 + FINAL_GAMES, step):
        tables = []
        for i in range(step):
            table = ResultsTable(page, 1, number_game + i, 'final_games')
            t = table.table
            tables.append(t)
        row_1.append(
            ft.Row(tables, wrap=True)
        )
        tables = []
        for i in range(step):
            table = ResultsTable(page, 2, number_game + i, 'final_games')
            t = table.table
            tables.append(t)
        row_2.append(
            ft.Row(tables, wrap=True)
        )

    left_grid = ft.Column(row_1)
    right_grid = ft.Column(row_2)
    lv_left.controls.append(left_grid)
    lv_right.controls.append(right_grid)
    row = ft.Row(
        [
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_left
            ),
            ft.VerticalDivider(),
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_right
            )
        ],
        spacing=0,
        expand=True,
        auto_scroll=True

    )

    cont_seating = ft.Container(
        content=ft.Row([row], expand=True)
    )
    # page.update()
    return cont_seating
