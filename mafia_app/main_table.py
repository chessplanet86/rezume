import flet as ft
from api import get_selections_or_finals_result, get_tournament_results, final_draw, final_comand
from config import COUNTS_GAME, FINAL_GAMES, ALL_COUNT_PEOPLE
import sqlite3


def trunc(x, y):
    x = x if x else 0
    if y != 0:
        return int(x / y * 1000) / 1000
    else:
        return 0


class SelectionTable:
    def __init__(self, page, table_data):
        self.page = page
        self.ref = ft.Ref[ft.DataTable]()
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1000,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Win"), numeric=True, tooltip="Число побед"),
                ft.DataColumn(ft.Text("Допы"), numeric=True, tooltip="Доп. баллы"),
                ft.DataColumn(ft.Text("ЛX"), numeric=True, tooltip="Лучший ход"),
                ft.DataColumn(ft.Text("Компенсация \nза ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("Дисквал"), numeric=True, tooltip="Баллы за дисквалификацию"),
                ft.DataColumn(ft.Text("Количество \n ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("ИТОГ"), numeric=True, tooltip='Итоговый баллы')
            ],
            rows=self.get_players_rows(self.change_selected)
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_selections_or_finals_result(count_games=COUNTS_GAME, order_str=None, table_from_base='games')
        rows_players = []
        for p in players:
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'], )),
                    ft.DataCell(ft.Text(trunc(p['result'], 1))),
                    ft.DataCell(ft.Text(p['extra_points'])),
                    ft.DataCell(ft.Text(p['best_move'])),
                    ft.DataCell(ft.Text(p['short_comp'])),
                    # ft.DataCell(ft.Text(f'{p["all_first_short"]} ({p["first_short_with_result"]})')),
                    ft.DataCell(ft.Text(p['discval'])),
                    ft.DataCell(ft.Text(f'{p["all_first_short"]}')),
                    ft.DataCell(ft.Text(p['total']))
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()


class FinalTable:
    def __init__(self, page, table_data):
        self.page = page
        self.ref = ft.Ref[ft.DataTable]()
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1000,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Win"), numeric=True, tooltip="Число побед"),
                ft.DataColumn(ft.Text("Допы"), numeric=True, tooltip="Доп. баллы"),
                ft.DataColumn(ft.Text("ЛX"), numeric=True, tooltip="Лучший ход"),
                ft.DataColumn(ft.Text("Компенсация \nза ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("Дисквал"), numeric=True, tooltip="Баллы за дисквалификацию"),
                ft.DataColumn(ft.Text("Количество \n ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("ИТОГ"), numeric=True, tooltip='Итоговый баллы')
            ],
            rows=self.get_players_rows(self.change_selected)
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_selections_or_finals_result(count_games=FINAL_GAMES, order_str=None,
                                                  table_from_base='final_games')
        rows_players = []
        for p in players:
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'], )),
                    ft.DataCell(ft.Text(trunc(p['result'], 1))),
                    ft.DataCell(ft.Text(p['extra_points'])),
                    ft.DataCell(ft.Text(p['best_move'])),
                    ft.DataCell(ft.Text(p['short_comp'])),
                    ft.DataCell(ft.Text(p['discval'])),
                    ft.DataCell(ft.Text(f'{p["all_first_short"]} ({p["first_short_with_result"]})')),
                    ft.DataCell(ft.Text(p['total']))
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()


class TournamentTable:
    def __init__(self, page, table_data):
        self.page = page
        self.ref = ft.Ref[ft.DataTable]()
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1000,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Win"), numeric=True, tooltip="Число побед"),
                ft.DataColumn(ft.Text("Допы"), numeric=True, tooltip="Доп. баллы"),
                ft.DataColumn(ft.Text("ЛX"), numeric=True, tooltip="Лучший ход"),
                ft.DataColumn(ft.Text("Компенсация \nза ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("Дисквал"), numeric=True, tooltip="Баллы за дисквалификацию"),
                ft.DataColumn(ft.Text("Количество \n ПУ"), numeric=True, tooltip="Первый убиенный"),
                ft.DataColumn(ft.Text("ИТОГ"), numeric=True, tooltip='Итоговый баллы')
            ],
            rows=self.get_players_rows(self.change_selected)
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_tournament_results()
        rows_players = []
        for p in players:
            extra_points = trunc(p['extra_points'], 1)
            best_move = trunc(p['best_move'], 1)
            short_comp = trunc(p['short_comp'], 1)
            discval = trunc(p['discval'], 1)
            total = trunc(p['total'], 1)
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(ft.Text(p['result'])),
                    ft.DataCell(ft.Text(extra_points)),
                    ft.DataCell(ft.Text(best_move)),
                    ft.DataCell(ft.Text(short_comp)),
                    ft.DataCell(ft.Text(discval)),
                    ft.DataCell(ft.Text(f'{p["all_first_short"]} ({p["first_short_with_result"]})')),
                    ft.DataCell(ft.Text(total))
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()


class SelectionTableStatistic:
    def __init__(self, page, table_data):
        self.page = page
        ref = ft.Ref[ft.DataTable]()
        self.ref = ref
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1800,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Мирный"), numeric=True, tooltip="Число игр мирным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за мирного",
                              on_sort=self._sort_mir),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_mir_sr),
                ft.DataColumn(ft.Text("Шериф"), numeric=True, tooltip="Число игр коммисаром (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за коммисара", on_sort=self._sort_kom),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_kom_sr),
                ft.DataColumn(ft.Text("Мафия"), numeric=True, tooltip="Число игр черным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за черного", on_sort=self._sort_black),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_black_sr),
                ft.DataColumn(ft.Text("Дон"), numeric=True, tooltip="Число игр доном (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за дона", on_sort=self._sort_don),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_don_sr),
                ft.DataColumn(ft.Text("Пистолет"), numeric=True, tooltip="Число игр пистолетом (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за пистолета",
                              on_sort=self._sort_pistolet),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного",
                              on_sort=self._sort_pistolet_sr),
            ],
            rows=self.get_players_rows(self.change_selected),
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_selections_or_finals_result(count_games=COUNTS_GAME, order_str=None, table_from_base='games')
        rows_players = []
        for p in players:
            red = trunc(p['red_extra'] + p['red_discvals'] + p['red_best_move'], 1)
            sr_red = trunc(red + p['red_win_games'], p['red_count_games'])
            kom = trunc(p['kommisar_extra'] + p['kommisar_discvals'] + p['kommisar_best_move'], 1)
            sr_kom = trunc(kom + p["kommisar_win_games"], p['kommisar_count_games'])
            black = trunc(p['black_extra'] + p['black_discvals'], 1)
            sr_black = trunc(black + p["black_win_games"], p['black_count_games'])
            don = trunc(p['don_extra'] + p['don_discvals'], 1)
            sr_don = trunc(don + p["don_win_games"], p['don_count_games'])
            pistolet = trunc(p['pistolet_extra'] + p['pistolet_discvals'], 1)
            sr_pistolet = trunc(pistolet + p["pistolet_win_games"], p['pistolet_count_games'])
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(ft.Text(f"{p['red_count_games']}  ({p['red_win_games']})")),
                    ft.DataCell(ft.Text(red)),
                    ft.DataCell(ft.Text(str(sr_red))),
                    ft.DataCell(ft.Text(str(p['kommisar_count_games']) + f' ({p["kommisar_win_games"]})')),
                    ft.DataCell(ft.Text(kom)),
                    ft.DataCell(ft.Text(str(sr_kom))),
                    ft.DataCell(ft.Text(str(p['black_count_games']) + f' ({p["black_win_games"]})')),
                    ft.DataCell(ft.Text(black)),
                    ft.DataCell(ft.Text(str(sr_black))),
                    ft.DataCell(ft.Text(str(p['don_count_games']) + f' ({p["don_win_games"]})')),
                    ft.DataCell(ft.Text(don)),
                    ft.DataCell(ft.Text(str(sr_don))),
                    ft.DataCell(ft.Text(str(p['pistolet_count_games']) + f' ({p["pistolet_win_games"]})')),
                    ft.DataCell(ft.Text(pistolet)),
                    ft.DataCell(ft.Text(str(sr_pistolet))),
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()

    def _sort_mir(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[2].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_mir_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[3].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[5].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[6].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[8].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[9].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[11].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[12].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[14].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[15].content.value))
        self.table.rows = new_rows
        e.page.update()


class FinalTableStatistic:
    def __init__(self, page, table_data):
        self.page = page
        ref = ft.Ref[ft.DataTable]()
        self.ref = ref
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1800,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Мирный"), numeric=True, tooltip="Число игр мирным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за мирного",
                              on_sort=self._sort_mir),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_mir_sr),
                ft.DataColumn(ft.Text("Шериф"), numeric=True, tooltip="Число игр коммисаром (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за коммисара", on_sort=self._sort_kom),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_kom_sr),
                ft.DataColumn(ft.Text("Мафия"), numeric=True, tooltip="Число игр черным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за черного", on_sort=self._sort_black),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_black_sr),
                ft.DataColumn(ft.Text("Дон"), numeric=True, tooltip="Число игр доном (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за дона", on_sort=self._sort_don),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_don_sr),
                ft.DataColumn(ft.Text("Пистолет"), numeric=True, tooltip="Число игр пистолетом (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за пистолета",
                              on_sort=self._sort_pistolet),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного",
                              on_sort=self._sort_pistolet_sr),
            ],
            rows=self.get_players_rows(self.change_selected),
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_selections_or_finals_result(count_games=FINAL_GAMES, order_str=None,
                                                  table_from_base='final_games')
        rows_players = []
        for p in players:
            red = trunc(p['red_extra'] + p['red_discvals'] + p['red_best_move'], 1)
            sr_red = trunc(red + p['red_win_games'], p['red_count_games'])
            kom = trunc(p['kommisar_extra'] + p['kommisar_discvals'] + p['kommisar_best_move'], 1)
            sr_kom = trunc(kom + p["kommisar_win_games"], p['kommisar_count_games'])
            black = trunc(p['black_extra'] + p['black_discvals'], 1)
            sr_black = trunc(black + p["black_win_games"], p['black_count_games'])
            don = trunc(p['don_extra'] + p['don_discvals'], 1)
            sr_don = trunc(don + p["don_win_games"], p['don_count_games'])
            pistolet = trunc(p['pistolet_extra'] + p['pistolet_discvals'], 1)
            sr_pistolet = trunc(pistolet + p["pistolet_win_games"], p['pistolet_count_games'])
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(ft.Text(f"{p['red_count_games']}  ({trunc(p['red_win_games'],1)})")),
                    ft.DataCell(ft.Text(red)),
                    ft.DataCell(ft.Text(str(sr_red))),
                    ft.DataCell(ft.Text(str(p['kommisar_count_games']) + f' ({p["kommisar_win_games"]})')),
                    ft.DataCell(ft.Text(kom)),
                    ft.DataCell(ft.Text(str(sr_kom))),
                    ft.DataCell(ft.Text(str(p['black_count_games']) + f' ({p["black_win_games"]})')),
                    ft.DataCell(ft.Text(black)),
                    ft.DataCell(ft.Text(str(sr_black))),
                    ft.DataCell(ft.Text(str(p['don_count_games']) + f' ({p["don_win_games"]})')),
                    ft.DataCell(ft.Text(don)),
                    ft.DataCell(ft.Text(str(sr_don))),
                    ft.DataCell(ft.Text(str(p['pistolet_count_games']) + f' ({p["pistolet_win_games"]})')),
                    ft.DataCell(ft.Text(pistolet)),
                    ft.DataCell(ft.Text(str(sr_pistolet))),
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()

    def _sort_mir(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[2].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_mir_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[3].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[5].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[6].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[8].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[9].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[11].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[12].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[14].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[15].content.value))
        self.table.rows = new_rows
        e.page.update()


class TournamentTableStatistic:
    def __init__(self, page, table_data):
        self.page = page
        ref = ft.Ref[ft.DataTable]()
        self.ref = ref
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1800,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("Ник"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text("Мирный"), numeric=True, tooltip="Число игр мирным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за мирного",
                              on_sort=self._sort_mir),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_mir_sr),
                ft.DataColumn(ft.Text("Шериф"), numeric=True, tooltip="Число игр коммисаром (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за коммисара", on_sort=self._sort_kom),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_kom_sr),
                ft.DataColumn(ft.Text("Мафия"), numeric=True, tooltip="Число игр черным (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за черного", on_sort=self._sort_black),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_black_sr),
                ft.DataColumn(ft.Text("Дон"), numeric=True, tooltip="Число игр доном (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за дона", on_sort=self._sort_don),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного", on_sort=self._sort_don_sr),
                ft.DataColumn(ft.Text("Пистолет"), numeric=True, tooltip="Число игр пистолетом (победил)"),
                ft.DataColumn(ft.Text("∑"), numeric=True, tooltip="Сумма баллов за пистолета",
                              on_sort=self._sort_pistolet),
                ft.DataColumn(ft.Text("Срд"), numeric=True, tooltip="Среднее за мирного",
                              on_sort=self._sort_pistolet_sr),
            ],
            rows=self.get_players_rows(self.change_selected),
        )
        page.update()

    def get_players_rows(self, change_selected):
        players = get_tournament_results()
        rows_players = []
        for p in players:
            red_discvals = p['red_discvals'] if p['red_discvals'] else 0
            red_best_move = p['red_best_move'] if p['red_best_move'] else 0
            red_extra = p['red_extra'] if p['red_extra'] else 0
            red_win_games = p['red_win_games'] if p['red_win_games'] else 0
            red_count_games = p['red_count_games'] if p['red_count_games'] else 0
            kommisar_extra = p['kommisar_extra'] if p['kommisar_extra'] else 0
            kommisar_discvals = p['kommisar_discvals'] if p['kommisar_discvals'] else 0
            kommisar_best_move = p['kommisar_best_move'] if p['kommisar_best_move'] else 0
            kommisar_win_games = p['kommisar_win_games'] if p['kommisar_win_games'] else 0
            kommisar_count_games = p['kommisar_count_games'] if p['kommisar_count_games'] else 0
            black_extra = p['black_extra'] if p['black_extra'] else 0
            black_discvals = p['black_discvals'] if p['black_discvals'] else 0
            black_win_games = p['black_win_games'] if p['black_win_games'] else 0
            black_count_games = p['black_count_games'] if p['black_count_games'] else 0
            don_extra = p['don_extra'] if p['don_extra'] else 0
            don_discvals = p['don_discvals'] if p['don_discvals'] else 0
            don_win_games = p['don_win_games'] if p['don_win_games'] else 0
            don_count_games = p['don_count_games'] if p['don_count_games'] else 0
            pistolet_extra = p['pistolet_extra'] if p['pistolet_extra'] else 0
            pistolet_win_games = p['pistolet_win_games'] if p['pistolet_win_games'] else 0
            pistolet_count_games = p['pistolet_count_games'] if p['pistolet_count_games'] else 0




            red = trunc(red_extra + red_discvals + red_best_move, 1)
            sr_red = trunc(red + red_win_games, red_count_games)
            kom = trunc(kommisar_extra + kommisar_discvals + kommisar_best_move, 1)
            sr_kom = trunc(kom + kommisar_win_games, kommisar_count_games)
            black = trunc(black_extra + black_discvals, 1)
            sr_black = trunc(black + black_win_games, black_count_games)
            don = trunc(don_extra + don_discvals, 1)
            sr_don = trunc(don + don_win_games, don_count_games)
            pistolet = trunc(pistolet_extra + pistolet_extra, 1)
            sr_pistolet = trunc(pistolet + pistolet_win_games, pistolet_count_games)
            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(ft.Text(f"{red_count_games}  ({red_win_games})")),
                    ft.DataCell(ft.Text(red)),
                    ft.DataCell(ft.Text(str(sr_red))),
                    ft.DataCell(ft.Text(str(kommisar_count_games) + f' ({kommisar_win_games})')),
                    ft.DataCell(ft.Text(kom)),
                    ft.DataCell(ft.Text(str(sr_kom))),
                    ft.DataCell(ft.Text(str(black_count_games) + f' ({black_win_games})')),
                    ft.DataCell(ft.Text(black)),
                    ft.DataCell(ft.Text(str(sr_black))),
                    ft.DataCell(ft.Text(str(don_count_games) + f' ({don_win_games})')),
                    ft.DataCell(ft.Text(don)),
                    ft.DataCell(ft.Text(str(sr_don))),
                    ft.DataCell(ft.Text(str(pistolet_count_games) + f' ({pistolet_win_games})')),
                    ft.DataCell(ft.Text(pistolet)),
                    ft.DataCell(ft.Text(str(sr_pistolet))),
                ],
                selected=False,
                on_select_changed=change_selected
            )
            rows_players.append(dataRow)
        return rows_players

    def change_selected(self, event):
        event.control.selected = not event.control.selected
        self.page.update()

    def _sort_mir(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[2].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_mir_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[3].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[5].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_kom_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[6].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[8].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_black_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[9].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[11].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_don_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[12].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[14].content.value))
        self.table.rows = new_rows
        e.page.update()

    def _sort_pistolet_sr(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[15].content.value))
        self.table.rows = new_rows
        e.page.update()


class TournamentCommandTable:
    def __init__(self, page, table_data):
        self.page = page
        self.ref = ft.Ref[ft.DataTable]()
        self.table = ft.DataTable(
            data=table_data,
            show_checkbox_column=True,
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1000,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("№"), tooltip="Место"),
                ft.DataColumn(ft.Text("Команда"), tooltip="Название команды"),
                ft.DataColumn(ft.Text("Очки"), numeric=True, tooltip="Число побед", on_sort=self._sort),
            ],
            rows=self.get_command_result()
        )
        page.update()

    def get_command_result(self):
        con = sqlite3.connect("mini_tournament.db")
        con.row_factory = sqlite3.Row
        players = final_comand()
        rows_players = []

        if len(players) == ALL_COUNT_PEOPLE:
            i = 0
            for p in range(0, ALL_COUNT_PEOPLE, 2):
                i += 1
                dataRow = ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text(str(i))),
                        ft.DataCell(ft.Text(players[p]['name_comand'])),
                        ft.DataCell(ft.Text(trunc(players[p]['command_total'] + players[p + 1]['command_total'],1))),
                    ],
                    selected=False,
                    on_select_changed=lambda e: e
                )
                rows_players.append(dataRow)
        con.close()
        return rows_players

    def _sort(self, e):
        new_rows = [*self.table.rows]
        new_rows = sorted(new_rows, key=lambda x: -float(x.cells[2].content.value))
        self.table.rows = new_rows
        e.page.update()
