import flet as ft
from all_players import PlayersTableSeating
from config import COUNTS_GAME, FINAL_GAMES, COUNT_PASS_GAMES


def container_seating(page):
    lv_left = ft.ListView(expand=True, spacing=10, padding=20)
    lv_right = ft.ListView(expand=True, spacing=10, padding=20)
    row_1 = [
        ft.Row([ft.Text(f"Жеребьевка 1 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)])
    ]

    row_2 = [
        ft.Row([ft.Text(f"Жеребьевка 2 стола", style=ft.TextThemeStyle.DISPLAY_MEDIUM)])
    ]

    step = 4
    for number_game in range(1, COUNTS_GAME + COUNT_PASS_GAMES + FINAL_GAMES + 1, step):
        tables = []
        for i in range(step):
            table = PlayersTableSeating(page, 1, number_game + i)
            t = table.table
            t.data_row_height = 20
            tables.append(t)
        row_1.append(
            ft.Row(tables, wrap=True)
        )
        tables = []
        for i in range(step):
            table = PlayersTableSeating(page, 2, number_game + i)
            t = table.table
            t.data_row_height = 20
            tables.append(t)
        row_2.append(
            ft.Row(tables, wrap=True)
        )

    left_grid = ft.Column(row_1)
    right_grid = ft.Column(row_2)
    lv_left.controls.append(left_grid)
    lv_right.controls.append(right_grid)
    row = ft.Row(
        [
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_left
            ),
            ft.VerticalDivider(),
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv_right
            )
        ],
        spacing=0,
        expand=True,
        auto_scroll=True

    )

    cont_seating = ft.Container(
        content=ft.Row([row], expand=True)
    )

    return cont_seating
