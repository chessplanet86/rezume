# Для сборки команда flet pack main.py

import flet as ft
from api import (
    get_players, get_max_tour,
    get_value_by_key, get_not_empty_extra_points,
    get_not_empty_discvals, save_game_result, init_db, get_all_players, get_gamers_sql,
    get_selections_or_finals_result, final_draw, get_seating_finals
)
from main_table import SelectionTable, FinalTable, TournamentTable, SelectionTableStatistic, \
    FinalTableStatistic, TournamentCommandTable, TournamentTableStatistic
from protocol_table import ProtocolTable
from all_players import get_all_players_table
from seating import container_seating
from results import container_results, container_results_final
import json
import sqlite3
import random
import numpy as np
from config import COUNTS_GAME, FINAL_GAMES, COUNT_PASS_GAMES, COUNT_PASS_PEOPLE, ALL_COUNT_PEOPLE
import shutil


def get_options(players):
    options = [ft.dropdown.Option(text=p['name'], key=p['id_player']) for p in players]
    options.append(ft.dropdown.Option(text='Подъем / Промах', key=404))
    return options


def set_initial_extra_points(refs):
    for _, ref in refs.items():
        ref.current.value = 0


def set_initial_discvals(refs):
    for _, ref in refs.items():
        ref.current.value = 0


def get_extra_points(players, refs):
    width = 140
    slider_one = []
    slider_two = []
    DOPS = (-10, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7)
    for i in range(5):
        options = [ft.dropdown.Option(text=f'{i / 10}' if i != 0 else '0', key=i) for i in DOPS]
        dropdown_body = {
            'label': f'Игрок {i + 1}',
            'autofocus': True,
            'width': width,
            'data': {'id_player': f'id Игрок {i + 1}'},
            'ref': refs[f'player_{i}'],
            'options': options
        }
        dropdown = ft.Dropdown(value=0, **dropdown_body)
        slider_one.append(dropdown)
    for i in range(5, 10):
        options = [ft.dropdown.Option(text=f'{i / 10}' if i != 0 else '0', key=i) for i in DOPS]
        dropdown_body = {
            'label': f'Игрок {i + 1}',
            'autofocus': True,
            'width': width,
            'data': {'id_player': f'id Игрок {i + 1}'},
            'ref': refs[f'player_{i}'],
            'options': options
        }
        dropdown = ft.Dropdown(value=0, **dropdown_body)
        slider_two.append(dropdown)

    return [ft.Row(slider_one, wrap=True), ft.Row(slider_two, wrap=True)]


def get_discvals(refs):
    width = 140
    discvals_one = []
    discvals_second = []
    for i in range(0, 5):
        dropdown = ft.Dropdown(
            label=f'Игрок {i + 1}',
            hint_text=f'Игрок {i + 1}',
            options=[ft.dropdown.Option(text='Нет', key=0), ft.dropdown.Option(text='Да', key=1)],
            autofocus=True,
            width=width,
            value=0,
            ref=refs[f'player_{i}'],
            data={'id_player': 'pass'}
        )
        discvals_one.append(dropdown)
    for i in range(5, 10):
        dropdown = ft.Dropdown(
            label=f'Игрок {i + 1}',
            hint_text=f'Игрок {i + 1}',
            options=[ft.dropdown.Option(text='Нет', key=0), ft.dropdown.Option(text='Да', key=1)],
            autofocus=True,
            width=width,
            value=0,
            ref=refs[f'player_{i}'],
            data={'id_player': 'pass'}
        )
        discvals_second.append(dropdown)

    return [ft.Row(discvals_one, wrap=True), ft.Row(discvals_second, wrap=True)]


def main(page):
    page.title = "Чердак. Мафия. Ханты-Мансийск"
    # Ссылки
    init_db()
    dropdown_pistole_one_ref = ft.Ref[ft.Dropdown]()
    dropdown_number_table_ref = ft.Ref[ft.Dropdown]()
    dropdown_pistole_two_ref = ft.Ref[ft.Dropdown]()
    dropdown_number_game_ref = ft.Ref[ft.Dropdown]()
    dropdown_don_ref = ft.Ref[ft.Dropdown]()
    dropdown_сommissar_ref = ft.Ref[ft.Dropdown]()
    dropdown_first_dead_ref = ft.Ref[ft.Dropdown]()
    selection_table_ref = ft.Ref[ft.DataTable]()
    selection_table_statistic_ref = ft.Ref[ft.DataTable]()
    tournament_table_statistic_ref = ft.Ref[ft.DataTable]()
    final_table_statistic_ref = ft.Ref[ft.DataTable]()
    final_table_ref = ft.Ref[ft.DataTable]()
    tournament_table_ref = ft.Ref[ft.DataTable]()
    command_result_table_ref = ft.Ref[ft.DataTable]()
    best_move_ref = ft.Ref[ft.TextField]()
    protocol_table_ref = ft.Ref[ft.DataTable]()
    right_grid_ref = ft.Ref[ft.Column]()
    results_ref = ft.Ref[ft.Tab]()
    results_final_ref = ft.Ref[ft.Tab]()
    selection_table = ft.Row([SelectionTable(page, {}).table], ref=selection_table_ref)
    selection_table_statistic = ft.Row([SelectionTableStatistic(page, {}).table], ref=selection_table_statistic_ref)
    tournament_table_statistic = ft.Row([TournamentTableStatistic(page, {}).table], ref=tournament_table_statistic_ref)
    final_table_statistic = ft.Row([FinalTableStatistic(page, {}).table], ref=final_table_statistic_ref)
    command_result_table = ft.Row([TournamentCommandTable(page, {}).table], ref=command_result_table_ref)
    tournament_table = ft.Row([TournamentTable(page, {}).table], ref=tournament_table_ref)
    final_table = ft.Row([FinalTable(page, {}).table], ref=final_table_ref)

    table_data = {'count_selected': 0, 'selected_rows': []}
    players = get_players()
    all_players = get_all_players()

    discvals_refs = {f"player_{p}": ft.Ref[ft.Dropdown]() for p in range(10)}
    extra_points_refs = {f"player_{p}": ft.Ref[ft.Dropdown]() for p in range(10)}

    def clear_result_tournament(e):
        con = sqlite3.connect("mini_tournament.db")
        cur = con.cursor()
        sql = """
        delete from games;
        """
        cur.execute(sql)
        sql = """
        delete from final_games;
        """
        cur.execute(sql)
        con.commit()

        selection_table_ref.current.controls[0] = SelectionTable(page, {}).table
        selection_table_statistic_ref.current.controls[0] = SelectionTableStatistic(page, {}).table
        tournament_table_statistic_ref.current.controls[0] = TournamentTableStatistic(page, {}).table
        final_table_statistic_ref.current.controls[0] = FinalTableStatistic(page, {}).table
        final_table_ref.current.controls[0] = FinalTable(page, {}).table
        command_result_table_ref.current.controls[0] = TournamentCommandTable(page, {}).table
        results_ref.current.content.content = container_results(page)
        results_final_ref.current.content.content = container_results_final(page)
        page.update()
        con.close()

    def get_seating(e):
        con = sqlite3.connect("mini_tournament.db")
        cur = con.cursor()
        sql = """
        select * from seating
        """

        if cur.execute(sql).fetchone():
            page.snack_bar.content.value = f'Жеребьевка команд отборочного тура уже произведена'
            page.snack_bar.open = True
            page.update()
            con.close()
            return

        sql = """
        select * from all_players
        """
        res = cur.execute(sql)

        res = res.fetchall()
        for j in range(COUNTS_GAME + COUNT_PASS_GAMES):
            table_1 = set()
            table_2 = set()
            for i in range(0, ALL_COUNT_PEOPLE, 2):
                if COUNT_PASS_PEOPLE:
                    if (j*COUNT_PASS_PEOPLE) % ALL_COUNT_PEOPLE <= i <= (j*COUNT_PASS_PEOPLE+COUNT_PASS_PEOPLE-1) % ALL_COUNT_PEOPLE:
                        continue

                r = random.randrange(1, 1000)
                if res[i][1] == 'pass':  # Здесь заглушка, когда нужно провести миник установить pass. Если есть участник, которого не может судить судья, вписать его ник
                    table_1.add(res[i + 1])
                    table_2.add(res[i])
                    continue

                if r % 2 == 1:
                    table_1.add(res[i])
                    table_2.add(res[i + 1])
                else:
                    table_1.add(res[i + 1])
                    table_2.add(res[i])

            table_1 = list(table_1)
            table_2 = list(table_2)
            dj_python_flag = 0
            arr_change_1 = []
            arr_change_2 = []
            was_change = 0
            for i in range(10):
                if table_1[i][1] in ('Асмэра', 'Hope'):
                    # dj_python_flag += 1
                    # arr_change_1.append([i, table_1[i]])
                    pass
            if dj_python_flag == 2:
                for i in range(10):
                    if table_2[i][1] == 'Поварец':
                        arr_change_2.append([i, table_2[i]])
                        break
                if arr_change_1[0][1][1] == 'Асмэра':
                    table_1[arr_change_1[0][0]] = arr_change_2[0][1]
                    table_2[arr_change_2[0][0]] = arr_change_1[0][1]
                    was_change = 1
                else:
                    table_1[arr_change_1[1][0]] = arr_change_2[0][1]
                    table_2[arr_change_2[0][0]] = arr_change_1[1][1]
                    was_change = 1

            if not was_change:
                dj_python_flag = 0
                arr_change_1 = []
                arr_change_2 = []
                for i in range(10):
                    if table_2[i][1] in ('Асмэра', 'Hope'):
                        # dj_python_flag += 1
                        # arr_change_1.append([i, table_2[i]])
                        pass
                if dj_python_flag == 2:
                    for i in range(10):
                        if table_1[i][1] == 'Поварец':
                            arr_change_2.append([i, table_1[i]])
                            break
                    if arr_change_1[0][1][1] == 'Асмэра':
                        table_2[arr_change_1[0][0]] = arr_change_2[0][1]
                        table_1[arr_change_2[0][0]] = arr_change_1[0][1]
                    else:
                        table_2[arr_change_1[1][0]] = arr_change_2[0][1]
                        table_1[arr_change_2[0][0]] = arr_change_1[1][1]

            np.random.shuffle(table_1)
            np.random.shuffle(table_2)
            np.random.shuffle(table_1)
            np.random.shuffle(table_2)
            np.random.shuffle(table_1)
            np.random.shuffle(table_2)
            for i in range(10):
                sql = f"""
                insert into seating(id_table, id_game, id_player, slot) values (1, {j}, {table_1[i][0]}, {i + 1})
                """
                cur.execute(sql)
                sql = f"""
                insert into seating(id_table, id_game, id_player, slot) values (2, {j}, {table_2[i][0]}, {i + 1})
                """
                cur.execute(sql)

        con.commit()
        con.close()

    def get_next_seating(e):
        con = sqlite3.connect("mini_tournament.db")
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        sql = """
        with res as (
        select distinct number_game, number_table from games) select count(*) cnt from res
        """
        res = cur.execute(sql).fetchone()
        cond = res['cnt'] < (COUNTS_GAME + COUNT_PASS_GAMES) * 2
        # cond = False  # Для теста
        if cond:
            page.snack_bar.content.value = f'Жеребьевка не возможна, т.к. не все результаты отборочных туров внесены'
            page.snack_bar.open = True
            page.update()
            con.close()
            return

        res = get_seating_finals()

        if not cur.execute(f"select * from seating where id_game={COUNTS_GAME + COUNT_PASS_GAMES}").fetchone():
            next_games = COUNTS_GAME + COUNT_PASS_GAMES + 1
            table_1 = []
            table_2 = []
            if len(res) != 20:
                page.snack_bar.content.value = f'Упс! Что-то пошло не так, возможно занесен только 1 стол 11 игры'
                page.snack_bar.open = True
                page.update()
                con.close()
                return

            for r in range(0, 20, 2):
                table_1.append(res[r]['id_player'])
                table_2.append(res[r + 1]['id_player'])


            np.random.shuffle(table_1)
            np.random.shuffle(table_2)

            for i in range(10):
                sql = f"""
                insert into seating(id_table, id_game, id_player, slot) values (1, {next_games - 1}, {table_1[i]}, {i + 1})
                """
                cur.execute(sql)
                sql = f"""
                insert into seating(id_table, id_game, id_player, slot) values (2, {next_games - 1}, {table_2[i]}, {i + 1})
                """
                cur.execute(sql)
            shutil.copy2('mini_tournament.db', f'final_mini_tournament_{next_games}.db')
        else:
            next_games_table_1 = cur.execute(
                "select number_game from final_games  where number_table=1 order by number_game desc").fetchone()
            next_games_table_2 = cur.execute(
                "select number_game from final_games where number_table=2 order by number_game desc").fetchone()
            if next_games_table_1 and next_games_table_2:
                if next_games_table_1['number_game'] != next_games_table_2['number_game']:
                    page.snack_bar.content.value = f'Ошибка! Результаты внесены не доконца.\nСтол 1 следующая игра {next_games_table_1["number_game"]}.Стол 2 следующая игра {next_games_table_2["number_game"]}'
                    page.snack_bar.open = True
                    page.update()
                    con.close()
                    return
            else:
                page.snack_bar.content.value = f'Ошибка! Результаты тура не заполнены или заполнены частично'
                page.snack_bar.open = True
                page.update()
                con.close()
                return

            final_res_draw = final_draw()
            next_tur = next_games_table_1['number_game'] + 1
            table_1 = []
            table_2 = []

            for r in range(0, 20, 2):
                table_1.append(final_res_draw[r]['id_player'])
                table_2.append(final_res_draw[r + 1]['id_player'])


            np.random.shuffle(table_1)
            np.random.shuffle(table_2)

            cond_seating_games = len(
                cur.execute(f"""select * from final_games where number_game = {next_tur}""").fetchall())
            cond_play_games = len(
                cur.execute(f"""select * from seating where id_game = {next_tur - 1}""").fetchall())
            if cond_seating_games == cond_play_games:
                # Условие что результаты тура внесены
                for i in range(10):
                    sql = f"""
                    insert into seating(id_table, id_game, id_player, slot) values (1, {next_tur - 1}, {table_1[i]}, {i + 1})
                    """
                    cur.execute(sql)
                    sql = f"""
                    insert into seating(id_table, id_game, id_player, slot) values (2, {next_tur - 1}, {table_2[i]}, {i + 1})
                    """
                    cur.execute(sql)
                shutil.copy2('mini_tournament.db', f'final_mini_tournament_{next_tur}.db')
            else:
                page.snack_bar.content.value = f'Ошибка! Результаты тура еще не внесены'
                page.snack_bar.open = True
                page.update()
                con.close()
                return

        con.commit()
        con.close()

    def set_initial_components():
        dropdown_don_ref.current.value = None
        dropdown_pistole_one_ref.current.value = None
        dropdown_pistole_two_ref.current.value = None
        dropdown_сommissar_ref.current.value = None
        dropdown_first_dead_ref.current.value = 404
        best_move_ref.current.value = 0
        set_initial_extra_points(extra_points_refs)
        set_initial_discvals(discvals_refs)

    def win_event(e, winner):
        try:
            page.client_storage.set('winner', winner)
            dialog_content = get_results(winner)
            dlg_modal.content.controls = [dialog_content]
            open_dlg_modal(e)
        except Exception as er:
            error_text = ''
            if str(er) == 'BL3':
                error_text = 'Черные указаны некорректно.'
            if str(er) == 'S=B':
                error_text = 'Черный и Шериф совпадают.'
            if str(er) == 'LH':
                error_text = 'Ошибка лучшего хода. Допы не могут быть начислены.'
            page.snack_bar.content.value = f'Упс, что-то пошло не так! {error_text}'
            page.snack_bar.open = True
            page.update()

    def get_data_page():
        don = dropdown_don_ref.current
        pistolet_1 = dropdown_pistole_one_ref.current
        pistolet_2 = dropdown_pistole_two_ref.current
        commisar = dropdown_сommissar_ref.current
        first_dead = dropdown_first_dead_ref.current
        best_move = best_move_ref.current
        number_game = int(dropdown_number_game_ref.current.value)
        number_table = int(dropdown_number_table_ref.current.value)
        return {'don': don, 'pistolet_1': pistolet_1, 'pistolet_2': pistolet_2, 'commisar': commisar,
                'best_move': best_move, 'first_dead': first_dead, 'number_game': number_game,
                'number_table': number_table}

    def close_dlg(e):
        dlg_modal.open = False
        dlg_modal.page.update()

    def close_and_sent_dlg(e):
        total_result_games = get_data_page()
        total_result_games['winner'] = page.client_storage.get('winner')
        total_result_games['extra_points'] = page.client_storage.get('extra_points')
        total_result_games['discvals'] = page.client_storage.get('discvals')
        save_game_result(total_result_games)
        set_initial_components()
        selection_table = SelectionTable(page, {})
        selection_table_statistic = SelectionTableStatistic(page, {})
        tournament_table_statistic = TournamentTableStatistic(page, {})
        final_table_statistic = FinalTableStatistic(page, {})
        final_table = FinalTable(page, {})
        tournament_table = TournamentTable(page, {})
        command_result_table = TournamentCommandTable(page, {})

        selection_table_ref.current.controls[0] = selection_table.table
        selection_table_statistic_ref.current.controls[0] = selection_table_statistic.table
        tournament_table_statistic_ref.current.controls[0] = tournament_table_statistic.table
        final_table_statistic_ref.current.controls[0] = final_table_statistic.table
        final_table_ref.current.controls[0] = final_table.table
        tournament_table_ref.current.controls[0] = tournament_table.table
        command_result_table_ref.current.controls[0] = command_result_table.table
        dlg_modal.open = False
        results_ref.current.content.content = container_results(page)
        results_final_ref.current.content.content = container_results_final(page)
        dlg_modal.open = False
        page.snack_bar.content.value = f'Сохранилось успешно!'
        page.snack_bar.open = True
        dlg_modal.page.update()

    def open_dlg_modal(e):
        page.dialog = dlg_modal
        dlg_modal.open = True
        page.update()

    def validation(p1, p2, don, sherif, first_dead, best_move):
        set_blacks = {p1, p2, don}
        if len(set_blacks) < 3:
            raise ValueError('BL3')
        if sherif in set_blacks:
            raise ValueError('S=B')
        if first_dead.value == 404 and float(best_move.value) != 0:
            raise ValueError('LH')

    def get_results(winner):
        winner_name = 'черных' if winner == 'black' else 'красных'
        game_data = get_data_page()
        pistolet_1 = game_data['pistolet_1']
        pistolet_2 = game_data['pistolet_2']
        game_result = ft.Row([ft.Text(f'Результат игры: Победа {winner_name}', style=ft.TextThemeStyle.TITLE_LARGE)],
                             wrap=True)
        pistolet_1 = get_value_by_key(pistolet_1.options, pistolet_1.value)
        pistolet_2 = get_value_by_key(pistolet_2.options, pistolet_2.value)
        don = game_data['don']
        don = get_value_by_key(don.options, don.value)
        sherif = game_data['commisar']
        sherif = get_value_by_key(sherif.options, sherif.value)
        first_dead = game_data['first_dead']
        first_dead = get_value_by_key(first_dead.options, first_dead.value)

        validation(pistolet_1, pistolet_2, don, sherif, game_data['first_dead'], game_data["best_move"])
        extra_points = get_not_empty_extra_points(extra_points_refs)
        not_empty_discvals = get_not_empty_discvals(discvals_refs)
        page.client_storage.set('extra_points', json.dumps(extra_points, ensure_ascii=False))
        page.client_storage.set('discvals', json.dumps(not_empty_discvals, ensure_ascii=False))
        blacks = ft.Row(
            [ft.Text(value=f'Команда черных: {don}, {pistolet_1}, {pistolet_2}', style=ft.TextThemeStyle.TITLE_LARGE)],
            wrap=True)
        commisar = ft.Row(
            [ft.Text(value=f'Коммисар игры: {sherif}', style=ft.TextThemeStyle.TITLE_LARGE)],
            wrap=True)
        first_dead = ft.Row(
            [ft.Text(value=f'Первый убиенный: {first_dead}', style=ft.TextThemeStyle.TITLE_LARGE)],
            wrap=True)
        discvals = ', '.join([not_empty_discvals[key]['label'] for key in not_empty_discvals])
        dialog_content = ft.Container(
            expand=True,
            content=ft.Column(
                [
                    game_result,
                    blacks,
                    commisar,
                    ft.Row([first_dead]),
                    ft.Row([ft.Text(f'Доп. за лучший ход: {game_data["best_move"].value}',
                                    style=ft.TextThemeStyle.TITLE_LARGE)]),
                    ft.Row([ft.Text(f'Дисквалифицированы: {discvals}', style=ft.TextThemeStyle.TITLE_LARGE)])

                ]
            )
        )
        return dialog_content

    def black_win_event(e):
        win_event(e, 'black')

    def red_win_event(e):
        win_event(e, 'red')

    def clear_protocol(e):
        pr_table = ProtocolTable(page)
        protocol_table_ref.current.controls[0] = pr_table.table
        protocol_table_ref.current.controls[1] = pr_table.dlg
        e.page.update()

    def get_gamers(e):
        control = e.control
        number_game = int(control.data[1].current.value)
        number_table = int(control.data[0].current.value)
        sql = f"""select * from seating where id_game = {number_game} - 1"""
        con = sqlite3.connect("mini_tournament.db")
        cur = con.cursor()
        if not cur.execute(sql).fetchone():
            con.close()
            page.snack_bar.content.value = f'Жеребьевка игры {number_game} отсутствует'
            page.snack_bar.open = True
            page.update()
            return

        sql = """delete from players"""
        cur.execute(sql)

        sql = get_gamers_sql(number_table, number_game)
        res = cur.execute(sql).fetchall()

        for r in res:
            sql = f"""
            insert into players(id, name, slot) values ({r[0]}, '{r[1]}', {r[2]})
            """
            cur.execute(sql)

        con.commit()

        pr_table = ProtocolTable(page)
        protocol_table_ref.current.controls[0] = pr_table.table
        protocol_table_ref.current.controls[1] = pr_table.dlg

        e.page.update()

        if not dropdown_pistole_one_ref.current.options:
            dropdown_pistole_one_ref.current.options = [ft.dropdown.Option(text=p[1], key=p[0]) for p in res]
        else:
            for r in range(10):
                dropdown_pistole_one_ref.current.options[r].text = res[r][1]
                dropdown_pistole_one_ref.current.options[r].key = res[r][0]

        if not dropdown_pistole_two_ref.current.options:
            dropdown_pistole_two_ref.current.options = [ft.dropdown.Option(text=p[1], key=p[0]) for p in res]
        else:
            for r in range(10):
                dropdown_pistole_two_ref.current.options[r].text = res[r][1]
                dropdown_pistole_two_ref.current.options[r].key = res[r][0]

        if not dropdown_don_ref.current.options:
            dropdown_don_ref.current.options = [ft.dropdown.Option(text=p[1], key=p[0]) for p in res]
        else:
            for r in range(10):
                dropdown_don_ref.current.options[r].text = res[r][1]
                dropdown_don_ref.current.options[r].key = res[r][0]

        if not dropdown_сommissar_ref.current.options:
            dropdown_сommissar_ref.current.options = [ft.dropdown.Option(text=p[1], key=p[0]) for p in res]
        else:
            for r in range(10):
                dropdown_сommissar_ref.current.options[r].text = res[r][1]
                dropdown_сommissar_ref.current.options[r].key = res[r][0]

        if not dropdown_first_dead_ref.current.options:
            options = [ft.dropdown.Option(text=p[1], key=p[0]) for p in res]
            options.append(ft.dropdown.Option(text='Подъем / Промах', key=404))
            dropdown_first_dead_ref.current.options = options
        else:
            for r in range(10):
                dropdown_first_dead_ref.current.options[r].text = res[r][1]
                dropdown_first_dead_ref.current.options[r].key = res[r][0]

        # обновление доп балов и дисквалов в зависимости от выбранных игроков
        for i in range(5):
            right_grid_ref.current.controls[-2].controls[i].data = {'id_player': res[i][0]}
            right_grid_ref.current.controls[-2].controls[i].label = res[i][1]
            right_grid_ref.current.controls[-2].controls[i].hint_text = res[i][1]

            right_grid_ref.current.controls[-5].controls[i].data = {'id_player': res[i][0]}
            right_grid_ref.current.controls[-5].controls[i].label = res[i][1]

        for i in range(5):
            right_grid_ref.current.controls[-1].controls[i].data = {'id_player': res[i + 5][0]}
            right_grid_ref.current.controls[-1].controls[i].label = res[i + 5][1]
            right_grid_ref.current.controls[-1].controls[i].hint_text = res[i + 5][1]

            right_grid_ref.current.controls[-4].controls[i].data = {'id_player': res[i + 5][0]}
            right_grid_ref.current.controls[-4].controls[i].label = res[i + 5][1]

        e.page.update()
        con.close()

    # Компоненты интерфейса

    black_win_button = ft.ElevatedButton("Победа мафии", on_click=black_win_event)
    red_win_button = ft.ElevatedButton("Победа мирных", on_click=red_win_event)

    ft.Text(f"Результаты турнира", style=ft.TextThemeStyle.DISPLAY_MEDIUM)
    header_protocol = ft.Text(f"Состав участников", style=ft.TextThemeStyle.DISPLAY_MEDIUM)
    header_role = ft.Text("Роли участников:", style=ft.TextThemeStyle.HEADLINE_MEDIUM)
    header_res_tour = ft.Text("Результаты тура:", style=ft.TextThemeStyle.HEADLINE_MEDIUM)
    header_dops = ft.Text("Дополнительные баллы:", style=ft.TextThemeStyle.HEADLINE_SMALL)
    header_discvals = ft.Text("Дисквалификация:", style=ft.TextThemeStyle.HEADLINE_SMALL)
    dropdown_pistole_one = ft.Dropdown(
        label="Пистолет 1",
        hint_text="Выберите второго мафа",
        options=[],
        autofocus=True,
        width=300,
        ref=dropdown_pistole_one_ref
    )

    dropdown_pistole_two = ft.Dropdown(
        label="Пистолет 2",
        hint_text="Выберите третьего мафа",
        options=[],
        autofocus=True,
        width=300,
        ref=dropdown_pistole_two_ref
    )

    dropdown_don = ft.Dropdown(
        label="Дон",
        hint_text="Выберите первого мафа",
        options=[],
        autofocus=True,
        width=300,
        ref=dropdown_don_ref
    )

    dropdown_сommissar = ft.Dropdown(
        label="Коммисар",
        hint_text="Выберите коммисара",
        options=[],
        autofocus=True,
        width=300,
        ref=dropdown_сommissar_ref
    )

    dropdown_first_dead = ft.Dropdown(
        label="Первый отстрел",
        hint_text="Выберите первого убиенного",
        options=[],
        autofocus=True,
        width=300,
        ref=dropdown_first_dead_ref,
        value=404
    )

    best_move_dropdown = ft.Dropdown(
        label="Количество черных в лучшем ходе",
        hint_text="Выберите количество черных в лх",
        options=[ft.dropdown.Option(text='0', key=0), ft.dropdown.Option(text='1', key='Получает компенсацию'),
                 ft.dropdown.Option(text='2', key=0.25),
                 ft.dropdown.Option(text='3', key='0.5')],
        autofocus=True,
        width=300,
        value=0,
        ref=best_move_ref
    )

    dlg_content = dict()
    dlg_content['dlg_content'] = ft.Text("Здесь должен быть чеклист")
    dlg_modal = ft.AlertDialog(
        modal=True,
        title=ft.Text("Пожалуйста, проверьте перед сохранением", style=ft.TextThemeStyle.HEADLINE_LARGE),
        content=ft.Column([ft.Text("Здесь")]),
        actions=[
            ft.TextButton("Сохранить", on_click=close_and_sent_dlg),
            ft.TextButton("Отмена", on_click=close_dlg),
        ],
        actions_alignment=ft.MainAxisAlignment.END,
        on_dismiss=lambda e: print("Modal dialog dismissed!"),
    )

    # Навигация по странице
    page.snack_bar = ft.SnackBar(
        content=ft.Text("Упс, что-то пошло не так! Возможно не все обязательные поля заполнены", height=50, size=30),
        action="Внимание!",
    )

    # Собираем статику

    lv = ft.ListView(expand=True, spacing=10, padding=20)
    right_grid = ft.Column(
        [
            header_role,

            dropdown_don,
            dropdown_pistole_one,
            dropdown_pistole_two,

            dropdown_сommissar,
            ft.Divider(),
            header_res_tour,
            ft.Row([dropdown_first_dead, best_move_dropdown], wrap=True),
            ft.Row([black_win_button, red_win_button], wrap=True),
            header_dops,
            *get_extra_points(players, extra_points_refs),
            header_discvals,
            *get_discvals(discvals_refs)
        ], ref=right_grid_ref)

    lv.controls.append(right_grid)

    protocol_table = ProtocolTable(page)

    number_table = dropdown_number_table_ref
    number_game = dropdown_number_game_ref
    # Основной интерфейс
    number_table_dropdown = ft.Dropdown(
        label="Выберите стол",
        hint_text="Выберите номер стола",
        options=[ft.dropdown.Option(text=1, key=1), ft.dropdown.Option(text=2, key=2)],
        autofocus=True,
        width=300,
        value=1,
        on_change=get_gamers,
        data=(number_table, number_game),
        ref=dropdown_number_table_ref
    )

    number_game_dropdown = ft.Dropdown(
        label="Выберите номер игры",
        hint_text="Выберите номер игры",
        options=[ft.dropdown.Option(text=i, key=i) for i in range(1, COUNTS_GAME + COUNT_PASS_GAMES + FINAL_GAMES + 1)],
        autofocus=True,
        width=300,
        value=1,
        on_change=get_gamers,
        data=(number_table, number_game),
        ref=dropdown_number_game_ref
    )

    row = ft.Row(
        [
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=ft.Column(
                    [
                        ft.Row(),
                        ft.Row([ft.Column(
                            [
                                ft.Row([number_table_dropdown]),
                                ft.Row([number_game_dropdown]),
                                ft.Row([ft.ElevatedButton(text="Получить состав на игру", on_click=get_gamers,
                                                          data=(number_table, number_game))])
                            ]
                        )],
                            wrap=True),
                        ft.Row([header_protocol], wrap=True),
                        ft.Row([protocol_table.table, protocol_table.dlg], wrap=True, ref=protocol_table_ref,
                               alignment='start')
                    ]
                )
            ),
            ft.VerticalDivider(),
            ft.Container(
                alignment=ft.alignment.center_left,
                expand=True,
                content=lv
            )
        ],
        spacing=0,
        expand=True,
        auto_scroll=True

    )

    # Таб
    lv_table_selection = ft.ListView(expand=True, spacing=10, padding=20)
    column = ft.Column(
        [
            ft.Row([ft.Text(f"Результаты отборочных игр", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([selection_table], wrap=True),
            ft.Row([ft.Text(f"Статистика отборочных игр", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([selection_table_statistic], wrap=True),
        ]
    )
    lv_table_selection.controls.append(column)

    lv_table_final = ft.ListView(expand=True, spacing=10, padding=20)
    column = ft.Column(
        [
            ft.Row([ft.Text(f"Результаты финальных игр", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([final_table], wrap=True),
            ft.Row([ft.Text(f"Статистика финальных игр", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([final_table_statistic], wrap=True),
        ]
    )
    lv_table_final.controls.append(column)

    lv_table_tournament = ft.ListView(expand=True, spacing=10, padding=20)
    column = ft.Column(
        [
            ft.Row([ft.Text(f"Результаты турнира", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([tournament_table], wrap=True),
            ft.Row([ft.Text(f"Командные результаты", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([ft.Text(f"*Чтобы отсортировать, необхдимо кликнуть заголовок ОЧКИ",
                            style=ft.TextThemeStyle.BODY_SMALL)], wrap=True),
            ft.Row([command_result_table], wrap=True),
            ft.Row([ft.Text(f"Статистика турнира", style=ft.TextThemeStyle.DISPLAY_MEDIUM)], wrap=True),
            ft.Row([tournament_table_statistic], wrap=True),
        ]
    )
    lv_table_tournament.controls.append(column)
    all_players_table = get_all_players_table(page)
    settings = [
        ft.Column([
            ft.Row([
                ft.Text(""),
                ft.ElevatedButton(text="Очистить содержимое результатов турнира", on_click=clear_result_tournament)
            ], wrap=True),
            ft.Row([
                ft.Text(""),
                ft.ElevatedButton(text="Провести жеребьевку команд отборочного тура", on_click=get_seating)
            ], wrap=True),
            ft.Row([
                ft.Text(""),
                ft.ElevatedButton(text="Провести жеребьевку команд следующего финального тура",
                                  on_click=get_next_seating)
            ], wrap=True)
        ],

        )
    ]
    t = ft.Tabs(
        selected_index=0,
        animation_duration=300,
        tabs=[
            ft.Tab(
                text="Участники турнира",
                content=ft.Container(
                    content=ft.Row([all_players_table], expand=True)
                ),
            ),
            ft.Tab(
                text="Рассадка",
                content=ft.Container(
                    content=container_seating(page)
                ),
            ),
            ft.Tab(
                text="Внесение результатов",
                content=ft.Container(
                    content=ft.Row([row], expand=True), alignment=ft.alignment.center
                ),
            ),
            ft.Tab(
                text="Результаты\nотбора",
                content=ft.Container(
                    content=container_results(page)
                ),
                ref=results_ref
            ),
            ft.Tab(
                text="Результаты\nфинала",
                content=ft.Container(
                    content=container_results_final(page)
                ),
                ref=results_final_ref
            ),
            ft.Tab(
                text="Таблица-Отбор",
                content=ft.Row([lv_table_selection], expand=True),
            ),
            ft.Tab(
                text="Таблица-Финалов",
                content=ft.Row([lv_table_final], expand=True),
            ),
            ft.Tab(
                text="Таблица-Турнира",
                content=ft.Row([lv_table_tournament], expand=True),
            ),
            ft.Tab(
                text="Настройки базы",
                content=ft.Row(settings),
            ),
        ],
        expand=1,
    )

    page.add(t)


ft.app(target=main, view=ft.FLET_APP)
