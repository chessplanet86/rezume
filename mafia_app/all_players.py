import flet as ft
from api import get_all_players, get_count_slots
from config import COUNTS_GAME


class AllPlayersTable:
    def __init__(self, page, number_table=None, number_game=None):
        self.ref = ft.Ref[ft.DataTable]()
        self.page = page
        self.chose_cell_event = None
        if not number_table:
            rows = self.get_rows()
            text = 'Ник'
        else:
            rows = self.get_rows(number_table, number_game)
            text = f'{number_game} Игра '
        self.table = ft.DataTable(
            data={},
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=1600,
            data_row_height=30,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("№"), numeric=True, tooltip="Игровой номер"),
                ft.DataColumn(ft.Text(f"{text}"), tooltip="Игровой ник"),
                ft.DataColumn(ft.Text(f"Команда"), tooltip="Команда"),
                ft.DataColumn(ft.Text(f"Клуб"), tooltip="Клуб"),
                ft.DataColumn(ft.Text(f"1 слот"), tooltip="1 слот"),
                ft.DataColumn(ft.Text(f"2 слот"), tooltip="2 слот"),
                ft.DataColumn(ft.Text(f"3 слот"), tooltip="3 слот"),
                ft.DataColumn(ft.Text(f"4 слот"), tooltip="4 слот"),
                ft.DataColumn(ft.Text(f"5 слот"), tooltip="5 слот"),
                ft.DataColumn(ft.Text(f"6 слот"), tooltip="6 слот"),
                ft.DataColumn(ft.Text(f"7 слот"), tooltip="7 слот"),
                ft.DataColumn(ft.Text(f"8 слот"), tooltip="8 слот"),
                ft.DataColumn(ft.Text(f"9 слот"), tooltip="9 слот"),
                ft.DataColumn(ft.Text(f"10 слот"), tooltip="10 слот"),
            ],
            rows=rows
        )
        page.update()

    def get_rows(self, number_table=None, number_game=None):
        players = get_all_players(number_table, number_game)
        rows_players = []
        n = 0

        for p in players:
            n += 1
            slots = get_count_slots(p['id_player'])
            transpons_slots = {f'{i}': 0for i in range(1, 11)}
            for slot in slots:
                if slot['slot'] == 0:
                    transpons_slots['10'] = str(slot['count'])
                else:
                    transpons_slots[str(slot['slot'])] = str(slot['count'])

            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(n)),
                    ft.DataCell(ft.Text(p['name'])),
                    ft.DataCell(ft.Text(p['name_comand'])),
                    ft.DataCell(ft.Text(p['club'])),
                    ft.DataCell(ft.Text(transpons_slots['1'])),
                    ft.DataCell(ft.Text(transpons_slots['2'])),
                    ft.DataCell(ft.Text(transpons_slots['3'])),
                    ft.DataCell(ft.Text(transpons_slots['4'])),
                    ft.DataCell(ft.Text(transpons_slots['5'])),
                    ft.DataCell(ft.Text(transpons_slots['6'])),
                    ft.DataCell(ft.Text(transpons_slots['7'])),
                    ft.DataCell(ft.Text(transpons_slots['8'])),
                    ft.DataCell(ft.Text(transpons_slots['9'])),
                    ft.DataCell(ft.Text(transpons_slots['10'])),
                ],
                selected=False
            )
            rows_players.append(dataRow)
        return rows_players


def get_all_players_table(page):
    table = AllPlayersTable(page)
    header_table = ft.Text(f"Таблица участников турнира", style=ft.TextThemeStyle.DISPLAY_MEDIUM)
    lv = ft.ListView(expand=True, spacing=10, padding=20)

    all_players = ft.Column(
        [
            ft.Row([header_table], wrap=True),
            ft.Row([table.table], wrap=True),
        ]
    )

    lv.controls.append(all_players)

    container = ft.Container(
        alignment=ft.alignment.center_left,
        expand=True,
        content=lv
    )

    return container


class PlayersTableSeating:
    def __init__(self, page, number_table=None, number_game=None):
        self.ref = ft.Ref[ft.DataTable]()
        self.page = page
        self.chose_cell_event = None
        if not number_table:
            rows = self.get_rows()
            text = 'Ник'
        else:
            rows = self.get_rows(number_table, number_game)
            text = f'{number_game} Игра '
        self.table = ft.DataTable(
            data={},
            border_radius=10,
            vertical_lines=ft.border.BorderSide(3, "blue"),
            horizontal_lines=ft.border.BorderSide(1, "green"),
            width=200,
            border=ft.border.all(2, "red"),
            columns=[
                ft.DataColumn(ft.Text("№"), numeric=True, tooltip="Игровой номер"),
                ft.DataColumn(ft.Text(f"{text}"), tooltip="Игровой ник"),

            ],
            rows=rows
        )
        page.update()

    def get_rows(self, number_table=None, number_game=None):
        players = get_all_players(number_table, number_game)
        rows_players = []
        n = 0
        for p in players:
            n += 1

            dataRow = ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(n)),
                    ft.DataCell(ft.Text(p['name'])),
                ],
                selected=False
            )
            rows_players.append(dataRow)
        return rows_players
