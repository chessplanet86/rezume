import sqlite3
import psycopg2

db = psycopg2.connect(database="postgres", user="postgres", password="masterkey", host="127.0.0.1", port="5432")
cursorPostgre = db.cursor()

res = cursorPostgre.execute("SELECT * from SKILBOX.cats")
cats = cursorPostgre.fetchall()
# print(cats)



connection = sqlite3.connect('db_cats.db')
cursor = connection.cursor()
cursor.execute("""
                create table cats (
                    id integer primary key not NULL, 
                    name text,
                    bread text, 
                    description text, 
                    age integer,
                    image text)
                """)

cursor.execute("""
                create table web_users(
                    id integer primary key not NULL, 
                    web_user text, 
                    password text)
                """)
                
cursor.execute(""" INSERT INTO web_users (web_user, password) VALUES (?, ?)""", ('admin', 'admin'))
cursor.execute(""" INSERT INTO web_users (web_user, password) VALUES (?, ?)""", ('user1', '123'))
cursor.execute(""" INSERT INTO web_users (web_user, password) VALUES (?, ?)""", ('user2', '123'))

for cat in cats:
    cursor.execute(""" INSERT INTO cats (name, bread, description, age, image) VALUES (?, ?, ?, ?, ?)""", (cat[1], cat[2], cat[3], int(cat[4]), cat[5]))
connection.commit()

a = cursor.execute("""SELECT * FROM cats""")
# print(a.description)
for i in a:
    print(i)


cursor.close()
db.close()
connection.close()