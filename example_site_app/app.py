# Авторизация

from flask import Flask, render_template, request, jsonify, abort, session, redirect, url_for, flash, g, send_file, Response
import jinja2
import re
import utils
import os
import config

app = Flask(__name__)
app.debug = False
app.secret_key = config.SECRET_KEY

@app.route("/")
@app.route("/main/<int:p>/")
def hello_world(p=1):
    print('Страница ', p)
    cats = utils.get_data_cats(p)
    return render_template("main/cats.html", cats = cats)




@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        if utils.check_password(request.form["username"], request.form["password"]):  
            return redirect("/")
        else:
            return render_template("session/login.html", failed=1)
    else:
        return render_template("session/login.html")

@app.route("/logout")
def logout():
    utils.clear_session()
    return redirect(url_for("login"))

@app.before_request
def before_request():
    """Выполняется перед каждым get/post запросом"""
    utils.connect_db()
    if "id" not in session and request.path!='/login':
        return redirect(url_for("login"))


@app.teardown_request
def teardown_request(exception):
    """
    Выполняется в конце каждого запроса.
    Отсоединяется от БД, удаляет временные файлы, если они есть.
    """
    utils.disconnect_db()
    try:
        for file_name in g.tempfiles:
            os.remove(file_name)
    except:
        pass




if __name__ == "__main__":
    app.debug = True
    #Для Докера
    # app.run(host='0.0.0.0') 
    app.run(host='127.0.0.1')